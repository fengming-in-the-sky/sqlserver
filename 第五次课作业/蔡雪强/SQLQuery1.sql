use master
go
if exists(select * from sys.databases where name='Student')
	drop database Student
create database Student
on
(
	name='Student',
	filename='D:\SQL\Student.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
log on
(
	name='Student_log',
	filename='D:\SQL\Student_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
go
use Student
create table StuInfo
(
stuNO char(5) primary key not null,
stuName nvarchar(20) not null,
stuAge int not null,
stuAddress nvarchar(200) not null,
stuSeat int not null,
stuSex nvarchar(1) default('0') check(StuSex='0' or StuSex='1'),
)
create table stuexam
(
examNO int primary key identity(1,1),
stuNO char(5) constraint FK_StuInfo_stuNO references StuInfo(stuNO),
writtenExam int not null,
labExam int not null
)
insert into StuInfo(stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex) values
('s2501','������',20,'�������',1,1),
('s2502','��˹��',18,'�����人',2,0),
('s2503','���Ĳ�',22,'���ϳ�ɳ',3,1),
('s2504','ŷ������',21,'�����人',4,0),
('s2505','÷����',20,'�����人',5,1),
('s2506','������',19,'�������',6,1),
('s2507','�·�',20,'�������',7,0)
insert into stuexam(stuNO,writtenExam,labExam) values('s2501',50,70),
('s2502',60,65),
('s2503',86,85),
('s2504',40,80),
('s2505',70,90),
('s2506',85,90)
select * from StuInfo
select stuNO ѧ��,stuName ����,stuAge ����,stuAddress ��ַ,stuSeat ��λ��,stuSex �Ա� from StuInfo
select stuName,stuAge,StuAddress from stuinfo
select stuNO as ѧ��,writtenExam as ����,labExam as ���� from stuexam 
select stuNO ѧ��,writtenExam ����,labExam ���� from stuexam  
select ѧ��=stuNO ,����=writtenExam,����=labExam  from stuexam
select stuName+'@'+stuAddress ���� from stuinfo
select stuNO ѧ��,writtenExam ����,labExam ����,writtenExam+labExam �ܷ� from stuexam
select distinct stuAddress from stuInfo
select distinct stuAge �������� from stuInfo
select top 3 * from stuinfo
select top 4 stuName ����,stuSeat ��λ�� from StuInfo
select top 50 percent * from stuinfo
select * from stuinfo where stuAddress in('�����人') and stuAge=20
select * from stuexam where labExam>=60 and labExam<=80  order by labExam DESC
select * from stuinfo where stuAddress in('�����人','���ϳ�ɳ')
select * from stuinfo where stuAddress='�����人' or stuAddress='���ϳ�ɳ'
select * from stuexam where writtenExam<70 or writtenExam>90 order by writtenExam ASC
select * from stuinfo where stuAge is null
select * from stuinfo where stuAge is not  null
select * from stuinfo where stuName like '��%'
select * from stuinfo where stuAddress like '��%'
select * from stuinfo where stuName like '��_'
select * from stuinfo where stuName like '__��%'
select * from stuinfo  order by stuAge DESC
select * from stuinfo order by stuAge DESC,stuSeat ASC
select top 1* from stuexam order by  writtenExam DESC
select top 1 * from stuexam order by labExam