use master
go
create database StuScore
on
(
	name='StuScore',
	filename='C:\学习\数据库\StuScore.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='StuScore_log',
	filename='C:\学习\数据库\StuScore_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use StuScore
go
Create table stuinfo
(
	stuNO varchar(20) primary key not null,
	stuName nvarchar(20) check(len(StuName)>=2),
	stuAge int check(StuAge>=0 AND StuAge<200) default(18) not null,
	stuAddress varchar(20) not null,
	stuSeat int identity(1,1),
	stuSex char(1) check(stuSex='1'or stuSex='0')
)
insert into stuinfo
select 's2501','张秋利','20','美国硅谷','1' union
select 's2502','李斯文','18','湖北武汉','0' union
select 's2503','马文才','22','湖南长沙','1' union
select 's2504','欧阳俊雄','21','湖北武汉','0' union
select 's2505','梅超风','20','湖北武汉','1'union
select 's2506','陈旋风','19','美国硅谷','1' union
select 's2507','陈风','20','美国硅谷','0'
select * from stuinfo


Create table stuexam
(
	examNO int primary key identity(1,1),
	stuNO varchar(20) constraint FK_stuexam_stuNO references stuinfo(stuNO),
	writtenExam int check(writtenExam>=0 AND writtenExam<=100),
	labExam int check(labExam>=0 AND labExam<=100)
)
insert into stuexam
select 's2501','50','70' union
select 's2502','60','65' union
select 's2503','86','85' union
select 's2504','40','80' union
select 's2505','70','90' union
select 's2506','85','90' 
select * from stuexam


select * from stuinfo
select 学生学号=stuNO,学生姓名=stuName,学生年龄=stuAge,学生地址=stuAddress,学生座位=stuSeat,学生性别=stuSex from stuinfo
select stuName,stuAge,stuAddress from stuinfo
select stuNO,writtenExam,labExam from stuexam
select 学号=stuNO,笔试=writtenExam,机试=labExam from stuexam
select stuNO 学号,writtenExam 笔试,labExam 机试 from stuexam
select stuNO as 学号,writtenExam as 笔试,labExam as 机试 from stuexam
select stuNO,stuName,stuAddress from stuInfo
select stuName+'@'+stuAddress 邮箱 from stuinfo
select stuNO 学号,writtenExam 笔试,labExam 机试,writtenExam+labExam 总分 from stuexam
select distinct stuAddress from stuInfo
select distinct stuAge 所有年龄 from stuInfo
select top 3 * from stuInfo
select top 4 stuName,stuSeat from stuInfo
select top 30 percent * from stuInfo
select * from stuInfo where stuAddress in ('湖北武汉') and stuAge in (20)
select * from stuExam where labExam>=60 and labExam<=80 order by labExam DESC
select * from stuInfo where stuAddress in ('湖北武汉') or stuAddress in ('湖南长沙')
select * from stuInfo where stuAddress in ('湖北武汉','湖南长沙') 
select * from stuExam where not writtenExam>=70 and writtenExam<=90 order by writtenExam ASC
select * from stuInfo where stuAge is null or stuAge=''
select * from stuInfo where stuAge is not null or not stuAge=''
select * from stuInfo where stuName like '张%'
select * from stuInfo where stuAddress like '%湖%'
select * from stuInfo where stuName like '张_'
select * from stuInfo where stuName like '__俊%'
select * from stuInfo order by stuAge DESC
select * from stuInfo order by stuAge DESC,stuSeat ASC
select top 1 * from stuExam  order by writtenExam DESC
select top 1 * from stuExam order by labExam ASC