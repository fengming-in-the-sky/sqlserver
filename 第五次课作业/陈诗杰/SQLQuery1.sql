use master
create database Student
go

use Student
create table StuInfo
(
	stuNO char(5) primary key,
	stuName nchar(5),
	stuAge int,
	stuAddress nchar(50),
	stuSeat int identity(1,1),
	stuSex char(1) check(stuSex = '1' or stuSex = '0')
)
go

use Student
create table StuExam
(
	examNO int identity(1,1) primary key,
	stuNO char(5) constraint FK_stuInfo_stuNO references StuInfo(stuNO),
	writtenExam int check(writtenExam >= 0 and writtenExam <= 100),
	labExam int check(labExam >= 0 and labExam <= 100)
)
go

use Student
insert into StuInfo(stuNO,stuName,stuAge,stuAddress,stuSex) values ('s2501','张秋利',20,'美国硅谷','1'),
('s2502','李斯文',18,'湖北武汉','0'),('s2503','马文才',22,'湖南长沙','1'),('s2504','欧阳俊雄',21,'湖北武汉','0'),
('s2505','梅超风',20,'湖北武汉','1'),('s2506','陈旋风',19,'美国硅谷','1'),('s2507','陈风',20,'美国硅谷','0')
go

use Student
insert into StuExam(stuNO,writtenExam,labExam) values ('s2501','50','70'),('s2502','60','65'),('s2503','86','65'),
('s2504','40','85'),('s2505','70','90'),('s2506','85','90')
go

select stuNO 学号,stuName 姓名, stuAge 年龄, stuAddress 地址,stuSeat 座位号,stuSex 性别 from StuInfo

select stuName,stuAge,stuAddress from StuInfo

select stuNO 学号,笔试 = writtenExam,labExam as 机试 from StuExam

select stuNO,stuName,stuAddress,邮箱 = stuName + '@' + stuAddress from StuInfo

select stuNo,writtenExam,labExam,总分 = writtenExam + labExam from StuExam

select distinct stuAddress from StuInfo

select stuAge, count(*) 所有年龄 from StuInfo group by stuAge

select top 3 * from StuInfo

select top 4 stuName,StuSeat from StuInfo

select top 50 percent * from StuInfo

select * from StuInfo where stuAddress = '湖北武汉' and stuAge = 20

select * from StuExam where labExam >= 60 and labExam <= 80 order by labExam desc
select * from StuExam where labExam between 60 and 80 order by labExam desc

select * from StuInfo where stuAddress = '湖北武汉' or stuAddress = '湖南长沙'
select * from StuInfo where stuAddress in ('湖北武汉' , '湖南长沙')

select * from StuExam where writtenExam < 70 or writtenExam > 90 order by writtenExam asc
select * from StuExam where writtenExam not between 70 and 90 order by writtenExam asc

select * from StuInfo where stuAge is null

select * from StuInfo where stuAge is not null

select * from StuInfo where stuName like '张%'

select * from StuInfo where stuAddress like'%湖%'

select * from StuInfo where stuName like '张_'

select * from StuInfo where stuName like '__俊%'

select * from StuInfo order by stuAge desc

select * from StuInfo order by stuAge desc, stuSeat asc

select top 1 * from StuExam order by writtenExam desc

select top 1 * from StuExam order by labExam asc

select * from StuExam where labExam = (select max(labExam) from StuExam)

select * from StuExam where writtenExam = (select min(writtenExam) from StuExam)
