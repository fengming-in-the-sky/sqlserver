use master
go

create database Students
on
(
	name='Students',
	filename='D:\sql���ݿ�\1\Students.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
log on
(
	name='Students_log',
	filename='D:\sql���ݿ�\1\Students_log.mdf',
	size=5,
	maxsize=50,
	filegrowth=10%
)
go

use Students
go

create table StuInfo
(
	StuNO varchar(5) primary key,
	StuName nvarchar(50) not null,
	StuAge int not null,
	StuAddress nvarchar(50) not null,
	StuSeat int not null,
	StuSex int default(1) check(StuSex='1' or StuSex='0')
)

insert into StuInfo values
('s2501','������','20','�������','1','1'),
('s2502','��˹��','10','�����人','2','0'),
('s2503','���Ĳ�','22','�人��ɳ','3','1'),
('s2504','ŷ������','21','�����人','4','0'),
('s2505','÷����','20','�����人','5','1'),
('s2506','������','19','�������','6','1'),
('s2507','�·�','20','�������','7','0')

create table ExamInfo
(
	examNO int primary key,
	StuNO varchar(5) foreign key references StuInfo(StuNO),
	WittenExam int,
	LabExam int
)

insert into ExamInfo values
('1','s2501','50','70'),
('2','s2502','60','65'),
('3','s2503','86','85'),
('4','s2504','40','80'),
('5','s2505','70','90'),
('6','s2506','85','90')

Select * from StuInfo
Select StuName,StuAge,StuAddress from StuInfo
select StuNO ѧ��,WittenExam ����,LabExam ���� from ExamInfo
select StuNO AS ѧ��,WittenExam AS ����,LabExam AS ���� from ExamInfo
select StuNO = 'ѧ��',WittenExam = '����',LabExam = '����' from ExamInfo
select StuNO,StuName,StuAddress,StuName+'@'+StuAddress AS '����' from StuInfo
select StuNO,WittenExam,LabExam,WittenExam+LabExam AS '�ܷ�' from ExamInfo
select StuAddress from StuInfo
select StuAge �������� from StuInfo
select top 3 * from StuInfo
select top 4 StuName,StuSeat from StuInfo
select top 50 percent * from StuInfo
select * from StuInfo where StuAddress='�����人' and StuAge=20
select * from ExamInfo where LabExam>=60 and LabExam<=80 order by LabExam
select * from StuInfo where StuAddress='�����人' or StuAddress='���ϳ�ɳ'
select * from StuInfo where StuAddress in('�����人','���ϳ�ɳ')
select * from ExamInfo where WittenExam<=70 or WittenExam>=90 order by WittenExam
select * from StuInfo where StuAge is null or StuAge=''
select * from StuInfo where StuAge is not null and not StuAge=''
select * from StuInfo where StuName like '��%'
select * from StuInfo where StuAddress like '��%'
select * from StuInfo where StuName like '��_'
select * from StuInfo where StuName like '__��%'
select * from StuInfo order by StuAge DESC
select * from StuInfo order by StuAge DESC,StuSeat ASC
select top 1 * from ExamInfo order by WittenExam
select top 1 * from ExamInfo order by LabExam ASC