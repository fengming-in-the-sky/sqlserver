use master
go
create database Students04
on
(
name= 'Students04',
filename='C:\test\Students04.mdf',
size=5MB,
maxsize=100MB,
filegrowth=10MB
)
log on
(
name='Students04_log',
filename='C:\test\Students04_log.ldf',
size=5MB,
maxsize=100MB,
filegrowth=10MB
)
create database Students
go
use Students
go

create table StuInfo(
	 stuNo nvarchar(20) primary key not null,
	 stuName nvarchar(10)not null,
	 stuAge int not null,
	 stuAddress nvarchar(20) null,
	 stuSeat int not null,
	 stuSex int check (stuSex=1 or stuSex=0)not null
)
go
insert into StuInfo(stuNo,stuName,stuAge,stuAddress,stuSeat,stuSex) values
('s2501','张秋利',20,'美国硅谷',1,1),('s2502','李斯文',18,'湖北武汉',2,0),
('s2503','马文才',22,'湖南长沙',3,1),('s2504','欧阳俊雄',21,'湖北武汉',4,0),
('s2505','梅超风',20,'湖北武汉',5,1),('s2506','陈旋风',19,'美国硅谷',6,1),
('s2507','陈风',20,'美国硅谷',7,0)
go
create table stuExam(
     examNo int primary key identity (1,1),
	 stuNo nvarchar(10) constraint [Fk_Exam_stuNo] foreign key([stuNo]) references [Student]([stuNO]) not null,
	 writtenExam int not null,
	 labExam int not null
)
go
insert into stuExam(stuNo,writtenExam,labExam) values
('s2501',50,70),('s2502',60,65),('s2503',86,85),
('s2504',40,80),('s2505',70,90),('s2506',85,90)
go
select stuNo as 学号 ,stuName as 姓名,stuAge as 年龄,stuAddress as 地址,stuSeat as 座位号,stuSex as 性别 from StuInfo
select stuName,stuAge,stuAddress from StuInfo
select stuNo as 学号,writtenExam as 笔试,labExam as 机试 from stuExam
select stuNo,stuName,stuAddress ,stuName+'@'+stuAddress as 邮箱 from StuInfo
select stuNo,writtenExam,labExam,writtenExam+labExam as 总分 from stuExam
select stuName, stuAddress from StuInfo
select stuAge as 年龄, count(*) as 所有年龄 from StuInfo group by stuAge
select top 3 * from StuInfo
select top 4 stuName as 姓名,stuSeat as 座位号 from StuInfo
select top 50 percent  * from StuInfo
select * from StuInfo where stuAddress='湖北武汉' and stuAge=20
select * from stuExam where labExam between 60 and 80 order by labExam desc
select * from StuInfo where stuAddress='湖北武汉' or stuAddress= '湖南长沙'
select * from StuInfo where stuAddress in ('湖北武汉','湖南长沙')
select * from stuExam where writtenExam not between 70 and 90 order by writtenExam asc
select * from StuInfo where stuAge=null or stuAge=' '
select * from StuInfo where stuAge is not null
select * from StuInfo where stuName like '%张%'
select * from StuInfo where stuAddress like '%湖%'
select * from StuInfo where stuName like '%张_'
select * from StuInfo where stuName like '%__俊%'
select * from StuInfo  order by stuAge desc 
select * from StuInfo order by stuAge desc,stuSeat 
select top 1 * from stuExam order by writtenExam desc
select top 1 * from stuExam order by labExam  

