use master
go

create database Students
on
(
	name='Students',
	filename='D:\sql数据库\Students.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name='Students_log',
	filename='D:\sql数据库\Students_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
go

use Students
go

create table Class
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) not null
)

create table Student
(
	StuID int primary key identity(1,1),
	ClassID int references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200),
)

create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(1<CourseCredit and CourseCredit<5)
)

alter table Student drop column StuAddress

alter table Student add StuAddress nvarchar(200)

alter table Class add constraint UQ_Class_ClassName unique(ClassName)

create table Score
(
	ScoreID int,
	StuID int,
	CourseID int,
	Score decimal(5,2) unique not null
)
alter table Score alter column ScoreID int not null
alter table Score add constraint PK_Score_ScoreID  primary key(ScoreID)
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)