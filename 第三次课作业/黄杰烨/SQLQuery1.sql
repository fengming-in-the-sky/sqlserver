
         use  master
		 go
create database Student
on
(
              name='student',
              filename='D:\test\Student.mdf'
)
log on
(
              name='student_log',
              filename='D:\test\Student.ldf'
)
    go 
        use  student
    go 
	 create  table  Class
(       
   ClassID    int primary key  identity(1,1),
   ClassName  nvarchar(20) unique   not null,   
        
 )

 use Student 
 go
  create  table    Student
(  StuID      int   primary key identity(1,1),
   ClassID    int  , 
   constraint FK_Student_ClassID   foreign key(ClassID ) references Class(ClassID ),
   StuName    nvarchar(20)  not null,
   StuSex     nvarchar(1)   default('��') check(StuSex in('��','Ů')),
   StuBirthday date, 
   StuPhone   nvarchar(11) unique
 )
 use Student
 go 
 create  table  Course
(  CourseID   int primary key ,
   CourseName nvarchar(50) unique  not null, 
)
use Student
go
create table Score
(  ScoreID  int  not null,
   StuID    int ,
   CourseID int ,
   Score    decimal(5,2)
)
 alter table Score add constraint Pk_Score_ScoreID primary key(ScoreID)

 alter table Score add constraint Fk_Score_StuID foreign key(StuID) references Student(StuID)
 
 alter table Score drop constraint Fk_Score_StuID 
  
 alter table Student add  StuAddress  nvarchar(200)

 alter table Course drop constraint Ck_Course_CourseCredit 

 alter table Course drop column CourseCredit

 alter table Course add CourseCredit int default(1)not null

 alter table Course add constraint Ik_Course_CourseCredit check(CourseCredit<6)

 