create database Student
go
use Student
go
create table Class
(
 --字段名 数据类型 设置主键 自增
 ClassID int primary key identity(1,1),
 ClassName nvarchar(20) unique not null,
)
create table Student
(
 StuID int primary key identity(1,1),
 --字段名 数据类型 设置外键关联class表的classID
 ClassID int foreign key (ClassID) references Class(ClassID),
 StuName nvarchar(20) not null,
 StuSex nvarchar(1) check(StuSex='男' or StuSex='女') not null,
 StuBirthday date,
 StuPhone nvarchar(11) unique,
)
create table Course
(
 CourseID int primary key identity(1,1),
 CourseName nvarchar(50) unique not null,
 CourseCredit int not null default('1') check(CourseCredit='1' or CourseCredit='1,2,3,4,5,')
)
create table Score
(
 ScoreID int identity(1,1),
 StuID int ,
 CourseID int,
 Score decimal(5,2) unique not null,
)
--学生信息表加字段
alter table Student add StuAddress nvarchar(200)
--成绩信息表添加约束
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)