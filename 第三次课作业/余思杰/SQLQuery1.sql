use master
create database Student
on primary
(
	name=Student,
	filename='D:\SQL作业\SQL作业2\Student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=1mb
)

log on
(
	name=Student_log,
	filename='D:\SQL作业\SQL作业2\Student_log.ldf',
	size=1mb,
	maxsize=10mb,
	filegrowth=10%
)
go

use Student
create table Class
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique(ClassName) not null
)
go

use Student
create table Student
(
	StuID int primary key identity(1,1),
	ClassID int constraint FK_Class_ClassID references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) check(StuSex in('男','女')),
	StuBirthday date,
	StuPhone nvarchar(11) unique(StuPhone)
)
go

use Student
create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique(CourseName) not null,
	CourseCredit int default(1) check(CourseCredit >= 1 and CourseCredit <= 5)
)
go

use Student
create table Score
(
	ScoreID int primary key identity(1,1),
	StuID int,
	CourseID int,
	Score decimal(5,2) unique(Score) not null
)
go

use Student
alter table Student add StuAddress nvarchar(200)
alter table Score add foreign key (StuID) references Student(StuID)
alter table Score add foreign key (CourseID) references Course(CourseID)


