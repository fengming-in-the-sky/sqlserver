use master
go
create database Student
go
use Student
go
create table Class
(
	ClassID int primary key identity(1,1),
	ClassNam nvarchar(20) unique not null

)
create table Student
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID) not null, 
	StuName nvarchar(20) not null,
	Stusex nvarchar(1) check(Stusex='��'or Stusex='Ů'),
	StuBirthday date ,
	StuPhone nvarchar(11) unique,
)
create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique,
	CourseCredit int not null default('1') check(CourseCredit='1'or CourseCredit='1,2,3,4,5')
)
create table Score
(
	ScoreID int identity(1,1),
	StuID int ,
	CourseID int,
	Score decimal(5,2) unique not null,
)
use Student
go
alter table Student add StuAddress nvarchar(200)
alter table Score add constraint PK_Score_ScoreID primary key (ScoreID)
alter table Score add constraint FK_Score_StuID foreign key (StuID) references Student (StuID)
alter table Score add constraint FK_Score_CourseID foreign key (CourseID) references Course (CourseID)