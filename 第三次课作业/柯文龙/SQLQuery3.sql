create database Student
on
(
	name='Student',
	filename='D:\SQL作业.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\SQL作业_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
go

use Student --切换数据库
go
create table Class(
    ClassId int primary key identity (1,1),
	ClassName nvarchar(20) constraint unique_1 unique(ClassName) not null  
	--设置唯一约束(constraint（约束）+约束名（自己可辨识就可以）+unique(列名))
)

go


create table Student(
    StuId int primary key identity (1,1),
	ClassId int constraint [Fk_Students_ClassId] foreign key([ClassId]) references [Class]([ClassId]) not null,
	--建表时创建外键关联约束（constraint +[约束名]+foreign key +([关联外键名])+references [外键表]([外键中的主键])）
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default'男'check (StuSex in('男','女')),
	StuBirthday date null, --date为时间专用字段类型
	StuPhone nvarchar(11) constraint unique_2 unique(StuPhone) null,
	
	
	)

	go

 alter table Student add  StuAddress nvarchar(20)
 --添加列固定语句 alter table +表名 add +列名+字段类型

 select * from Student--确定列是否添加成功

create table Course(
    CourseId int primary key identity (1,1),
	CourseName nvarchar(50) constraint unique_3 unique(CourseName) not null,
	CourseCredit int default 1 check (CourseCredit>=1 and CourseCredit<=5)

	--default 为默认值 check括号内为取值范围，该列的取值范围表达为列名>=x and 列名<=y
)

create table Score(
    ScoreID int not null,
	StuId int constraint [Fk_Score_StuId] foreign key([StuId]) references [Students]([StuId]) not null,
	CourseId int not null,
	Score decimal(5,2) constraint unique_4 unique(Score) not null

)

alter table Score add constraint PK_ScoreId primary key (ScoreId)

--添加主键约束，alter table 表名 add constraint 约束名 primary key (字段名)

alter table Score add constraint [Fk_Score_CourseId] foreign key([CourseId]) references [Course]([CourseId])

--建表后添加外键约束，alter table 表名 add constraint +[约束名]+foreign key +([关联外键名])+references [外键表]([外键中的主键])



