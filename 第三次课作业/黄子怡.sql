use master
go
create database Students02
on
(
  name='Students02',
  filename='C:\test\Students02.mdf',
  size=5MB,
  maxsize=50MB,
  filegrowth=10%
  )
 
    log on
 (
    name='Students02_log',
  filename='C:\test\Students02_log.ldf',
  size=5MB,
  maxsize=50MB,
  filegrowth=10%
  )
  go
  use Students02
  go
  create table ClassInfo
  (ClassID int primary key identity(1,1), 
  ClassName nvarchar(20) unique not null,
  )
   create table StudentInfo
   (StuID int primary key identity(1,1),
   ClassID int foreign key references ClassInfo(ClassID),
  StuName nvarchar(20) not null,
  StuSex nvarchar(1) default ('��')check(StuSex='��' or StuSex='Ů') not null,
  StuBirthday date,
  StuPhone nvarchar(11) unique,
  )
   create table CourseInfo
   (CourseID int primary key identity (1,1),
   CourseName nvarchar(50) unique,  
   CourseCredit int default('1') check(CourseCredit='1' or CourseCredit='1,2,3,4,5,')not null,
   )
create table ScoreInfo
(ScoreID int  identity (1,1),
 StuID int , 
 CourseID int  ,
 Score decimal(5,2) unique not null,
)
alter table StudentInfo add StuAddress nvarchar(200)
alter table ScoreInfo add constraint PK_ScoreInfo_ScoreID primary key(ScoreID)
alter table ScoreInfo add constraint FK_ScoreInfo_StuID foreign key(StuID) references ScoreInfo(StuID)
alter table ScoreInfo add constraint FK_ScoreInfo_CourseID foreign key(CourseID) references  CourseInfo(CourseID)
