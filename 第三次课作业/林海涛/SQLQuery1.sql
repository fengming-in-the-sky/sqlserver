use master
go

create database Student
on
(
	name = 'Student',
	filename = 'D:\数据库\Student.mdf'
)

log on
(
	name = 'Student_log',
	filename = 'D:\数据库\Student.ldf'
)

use Student
go

create table Class
(
	ClassID int primary key,
	ClassName nvarchar(20) unique not null
)

create table Student
(
	StuID int primary key identity (1,1),
	ClassID int references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) check( StuSex in('男','女')),
	StuBirthday date ,
	StuPhone nvarchar(11) unique, 
)

create table Course
(
	CourseID int identity(1,1) primary key,
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default('1') check(CourseCredit in(1,2,3,4,5))
) 

create table Score
(
	ScoreID int identity(1,1),
	StuID int ,
	CourseID int ,
	Score decimal(5,2) unique not null
)

alter table Student add StuAddress nvarchar(20)
alter table Score add constraint pk_ScoreID primary key (ScoreID)
alter table Score add constraint fk_StuID foreign key (StuID) references Student(StuID)
alter table Score add constraint fk_CourseID foreign key (CourseID) references Course(CourseID)
