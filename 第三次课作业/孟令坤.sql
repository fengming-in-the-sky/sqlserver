use master
go
create database Students
on primary
(
   name='Students',
   filename='D:\TEXT\Students.mdf',
   size=5MB,
   maxsize=100MB,
   filegrowth=10Mb
)
log on
(
   name='Students_log',
   filename='D:\TEXT\Students_log.ldf',
   size=5MB,
   maxsize=100MB,
   filegrowth=10Mb
)
go

use Students
go

create table Class
(
  ClassID int primary key identity(1,1),
  ClassName nvarchar(20) unique(ClassName) not null
)
go

use Students
go

create table Student
(
  StuID int primary key identity(1,1),
  ClassID int references Class(ClassID),
  StuName nvarchar(20) not null,
  StuSex nchar(1) default('��') check(StuSex in('��','Ů')),
  StuBirthday date,
  StuPhone nvarchar(11) unique(StuPhone)
)
go

use Students
go

create table Course
(
  CourseID int primary key identity(1,1),
  CourseName nvarchar unique(CourseName) not null,
  CourseCredit int default(1) check(CourseCredit>=1 or CourseCredit<=5)
)
go
use Students
go
create table Score
(
  ScoreID int identity(1,1),
  StuID int,
  CourseID int,
  Score decimal(5,2) unique not null
)
go
use Students
go
alter table Student add StuAddress nvarchar
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)