use master
go

create database Student
on
(
	name='Student',
	filename='D:\test\Student.mdf',
	size=5MB,
	maxsize=15MB,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\test\Student_log.ldf',
	size=5MB,
	maxsize=15MB,
	filegrowth=10%
)
go

use Student
go

create table Class
(
	ClassID int  identity(1,1),
	ClassName nvarchar(20) unique not null
)
create table Student
(
	StuID int  identity(1,1),
	ClassID int,
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) check(StuSex in('��','Ů')),
	StuBirthday date,
	StuPhone nvarchar(11) unique
)
create table Course
(
	CourseID int identity(1,1),
	CourseName nvarchar(50) unique,
	CoueseCredit int default(1) check(CoueseCredit in(1,2,3,4,5))
)
create table Score
(
	ScoreID int identity(1,1),
	StuID int ,
	CourseID int,
	Score decimal(5,2) unique
)
go

use Student
go

alter table Student add StuAddress nvarchar(200)

alter table Class add constraint PK_Class_ClassID primary key(ClassID)

alter table Student add constraint PK_Student_StuID primary key(StuID)

alter table Student add constraint FK_Student_ClassName foreign key(StuID) references Class(ClassID)

alter table Course add constraint PK_Course_CourseID primary key(CourseID)

alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)

alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)

alter table CourseID add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)