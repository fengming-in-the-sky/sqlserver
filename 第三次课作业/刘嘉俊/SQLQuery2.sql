Create database Student
on(
name = 'Student',
filename ='D:\sqlzuoye.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10MB
)
log on
(
name = 'Student_log',
filename ='D:\Student_log.ldf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10MB
)
go

use Student
go

create table Class
(
ClassID int primary key identity(1,1),
ClassName nvarchar(20) unique not null
)

create table Student
(
StuID int primary key identity(1,1),
ClassID int foreign key references Class(ClassID),
StuName nvarchar(20) not null,
StuSex nvarchar(20) default('��')  check(StuSex='��' or  StuSex='Ů'),
StuBirthday date,
StuPhone nvarchar(11) unique
)

create table Course
(
CourseID int primary key identity(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int default('1') check(CourseCredit >= 1 and CourseCredit <= 5)
)

create table Score
(
ScoreID int ,
StuID int ,
CourseID int ,
Score decimal(5,2) unique not null
)

use Student
go

alter table Student add StuAddress nvarchar(200)
alter table Score alter column ScoreID int not null 
alter table Score add constraint PK_ScoreID primary key(ScoreID)
alter table Score add constraint CK_StuID foreign key (StuID) references Student(StuID)
alter table Score add constraint DF_CourseID foreign key (CourseID) references Course(CourseID) 

