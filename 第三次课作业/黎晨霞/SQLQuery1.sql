use master
go

create database Student
on
(
  name='Student',
  filename='F:\Student.mdf',
  size=5MB,
  maxsize=50MB,
  filegrowth=10%
)
log on
(
  name='Student_log',
  filename='F:\Student_log.ldf',
  size=5MB,
  maxsize=50MB,
  filegrowth=10%
)
go

use Student
go

create table Class
(
    ClassID int primary key identity(1,1) not null,
	ClassName nvarchar(20) unique not null--
)
create table Student
(
    StuID int primary key identity(1,1) not null,
	ClassID int references Class(ClassID) ,--
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) Check( StuSex in('��','Ů')),
	StuBirthday date ,
	StuPhone nvarchar(11) unique,--
	StuAddress nvarchar(20)--!!
)
create table Course
(
    CourseID int primary key identity(1,1) not null,
	CourseName nvarchar(50) unique,--
	CourseCredit int default(1) check(CourseCredit>=1 or CourseCredit<=5) --
)
create table Score--!!
(
    ScoreID int  primary key identity(1,1)not null,--!!
	StuID int references Student(StuID) ,--
	CourseID int references Course(CourseID),--
	Score decimal(5,2) unique not null--
)