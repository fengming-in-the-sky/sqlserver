use master
go
create database bbs
on
(
name='bbs',
filename='D:\testbbs.mdf',
size=5mb,
maxsize=500mb,
filegrowth=1mb
),
(
name='bbs',
filename='D:\testbbs_log.ldf',
size=5mb,
maxsize=500mb,
filegrowth=1mb
)
create table bbsusers 
(
uid int primary key identity(1,1),
uname varchar(10)  unique not null,
usex varchar(2) not null check(usex in('男','女')),
uage int not null check(uage>=15 and uage<=60),
upoint int not null check(upoint>=0)
)
create table bbssection 
(
sid int primary key identity(1,1),
sname varchar(10) not null,
suid int foreign key references bbsusers(uid)
)
create table bbstopic
(
tid int primary key identity(1,1),
tuid int references bbsusers(uid),
tsid int references bbssection(sid),
ttitle varchar(100) not null,
tmsg text not null,
ttime datetime not null,
tcount int
)
create table bbsreply
(
rld int primary key identity(1,1),
ruld int foreign key references bbsusers(uid),
tsld int foreign key references bbssection(sid),
rmsg text not null,
rtime datetime
)
insert into bbsusers(uname,usex,uage,upoint)
values('小雨点','女',20,0)
insert into bbsusers(uname,usex,uage,upoint)
values('逍遥','男',18,4)
insert into bbsusers(uname,usex,uage,upoint)
values('七年级生','男',19,2)
select * from bbsusers
insert into bbssection(sname,suid)
values('技术交流',1)
insert into bbssection(sname,suid)
values('读书世界',3)
insert into bbssection(sname,suid)
values('生活百科',1)
insert into bbssection(sname,suid)
values('八卦区',3)
select * from bbsusers
insert into bbsTopic values
(2,4,'范跑跑 ',' 谁是范跑跑  ','2008-7-8','1'),
(3,1,'.NET  ',' 与JAVA的区别是什么呀？  ','2008-9-1 ','2'),
(1,3,'今年夏天最流行什么 ',' 有谁知道今年夏天最流行  ','2008-9-10','0')
insert into bbsreply(ruld,rmsg,rtime) values(1,'我就是','2008-7-8')
insert into bbsreply(ruld,rmsg,rtime) values(6,'哦flak积分','2008-7-8')
insert into bbsreply(ruld,rmsg,rtime) values(1,'爱科技','2008-7-8')
select * from bbsUsers
delete  bbsusers where  uname ='逍遥'
alter table bbssection drop NK
alter table bbstopic drop FK__bbstopic__tuid__5315624
update bbsusers set upoint ='10' where uname='小雨点'

select * from bbssection
delete bbssection where sname='生活百科'

select * from bbsreply
delete bbsreply