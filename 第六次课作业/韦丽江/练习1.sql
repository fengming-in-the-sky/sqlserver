--用SQL实现以下的题目：

--1. 先创建一个数据库，数据库名为TestDB，要求有两个数据文件，一个日志文件，注意命名规范，文件存放在E盘下
--   再在上面的数据库下创建表，结构如下：
use master
go


create database TestDB
 
on(
	name='TestDB.mdf',
	filename='D:\tex.\TestDB.mdf',
	size=8mb,
	maxsize=80mb,
	filegrowth=10%
)

log on(
	name='TesDB_log',
	filename='D:\tex.\TesDB_log.ldf',
	size=8mb,
	maxsize=80mb,
	filegrowth=10%
)
go
use TestDB
go

create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null,

)
go

create table loginInfo
(
	LoginId int primary key identity(1,1),
	LoginName varchar(10) not null unique ,
	LoginPwd varchar(20) not null default('123456'),
	typeSex nvarchar(1) default('男') check(typeSex='男' or typeSex='女' ),
	typebirthday datetime not null,
	typestle nvarchar(6) not null

)
	
	
--   会员类别表(typeInfo)：
--   类别编号(typeId)：主键、自动编号
--   类别名(typeName): varchar(10)  不能为空

--   登录用户表(loginInfo)：
--   编号(LoginId)，数据类型(int)，主键、自动编号
--   账户(LoginName)，文本，长度为10，非空，必须唯一，不能重复
--   密码(LoginPwd)，文本，长度为20，非空、默认值为‘123456’
--   性别(自定类型)
--   生日(自定类型)
--   会员类别(自定类型)


--2. 先创建一个数据库用来存放某公司的员工信息，数据库的名称为company，包含2个数据文件1个日志
--文件，数据文件和日志文件全部存放在E盘中，初始大小，增长和最大大小自己设定
--   再创建表：
create database company
on(
	name='company.mdf',
	filename='D:\tex.tex\company.mdf',
	size=8mb,
	maxsize=80mb,
	filegrowth=10%

)
log on
(
	name='company_log',
	filename='D:\tex.tex\company_log.ldf',
	size=8mb,
	maxsize=80mb,
	filegrowth=10%
)
go

use company
go

create table sectionInfo
(
	sectionID int primary key ,
	sectionName  varchar(10) not null,
)
go

create table userInfo
(
	userNo int primary key not null,
	 userName varchar(10) unique not null  check (userName>=4),
	 userSex varchar(2) not null default('男') check(userSex='男' or userSex='女' ),
	 userAge int not null check(userAge>=1 and userAge<=100),
	 userAddress varchar(50) default('湖北'),
	 userSection  int references sectionInfo(sectionID)

)




--	部门信息表（sectionInfo）
--	部门编号  sectionID  int 标识列  主键
--	部门名称  sectionName  varchar(10)  不能为空 check


--	员工信息表（userInfo）
--	员工编号  userNo  int  标识列 主键  不允许为空
--	员工姓名  userName  varchar(10)  唯一约束 不允许为空 长度必须大于4
--	员工性别  userSex   varchar(2)  不允许为空  只能是男或女
--	员工年龄  userAge   int  不能为空  范围在1-100之间
--	员工地址  userAddress  varchar(50)  默认值为“湖北”
--	员工部门  userSection  int  外键，引用部门信息表的部门编号





--	员工考勤表（workInfo）
--	考勤编号  workId int 标识列  主键  不能为空
--	考勤员工  userId  int 外键 引用员工信息表的员工编号 不能为空
--	考勤时间  workTime datetime 不能为空
--	考勤说明  workDescription  varchar(40) 不能为空 内容只能是“迟到”，“早退”，“旷工”，“病假”，“事假”中的一种
create table workInfo
(
	workId int  primary key not null,
	userId int references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription varchar(40) not null check(workDescription='迟到'or workDescription='早退' or workDescription='旷工' or workDescription='病假',or workDescription='事假'),

)




--3. 为学校开发一个学生管理系统，请为该系统创建数据库，主要存放的信息有：班级信息、学生信息、课程信息、学生考试成绩
create database StudentOrder
on(
	name='StudentOrder.mdf',
	filename='D:\tex.\StudentOrder.mdf',
	size=6mb,
	maxsize=60mb,
	filegrowth=10%
)
log on(

	name='StudentOrder_log',
	filename='D:\tex.tex\StudentOrder_log.ldf',
	size=6mb,
	maxsize=60mb,
	filegrowth=10%

)
go
use StudentOrder
go

create table Classinfo
(
	classid int primary key identity(1,1),
	ClassName nvarchar(5)  not null unique,
	ClassTime  datetime not null,
	Classdescribe text

)

--   班级信息：班级编号 classid (主键、标识列)
--             班级名称(例如：T1、T2、D09等等):不能为空，不能重复
--             开办时间：不能为空
--             班级描述

--   学生信息：学号：主键、标识列
--             姓名：长度大于2，不能重复
--             性别：只能是‘男’或‘女’，默认为男，不能为空
--             年龄：在15-40之间，不能为空
--             家庭地址：默认为“湖北武汉”
--             所在的班级编号

create table Studentinfo

(
	StudentId int primary key identity(1,1),
	StudentName varchar(10) unique  check (StudentName>=2),
	StudentSex varchar(2) default('男') check(StudentSex='男' or StudentSex='女') not null,
	StudentAge varchar(100) check(StudentAge>=15 and StudentAge<=40) not null,
	StudentAddress text default('湖北武汉'),
	ClassId varchar(5) default('软件2班') not null,
)

--   课程信息：编号：主键、标识列
--             课程名：不能为空，不能重复
--             课程描述：
create table coursemessage
 
 (
	courseid int primary key identity(1,1),
   coursename nvarchar(5)not null,
       coursedescribe     text
 )
   
--   成绩信息：成绩编号：主键、标识列
--             成绩所属于的学生编号，不能为空
--             成绩所属的课程编号，不能为空
--             成绩：在0-100之间

create table scoreinfo
(
	scoreid int primary key identity(1,1),
	 scorestuid int references Studentinfo(StudentId) not null,
     scorestu int references coursemessage(courseid) not null,
  score int check(score>=0 or score<=100)
)

--4. 为一个房屋出租系统创建一个数据库，数据库中主要存放房屋的信息，包括：房屋的编号，房屋的描述，发布人的信息(姓名和联系电话)，房屋的租金，房屋的室数，房屋的厅数，房屋的类型(别墅、普通住宅、平房、地下室)，房屋所属的区县(武昌、汉阳、汉口)，请根据上面的描述设计表，并设计表的关系，以及列的约束

create database fangwu
on(
  name='fangwu',
   filename='D:\tex.\fangwu.mdf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
log on 
(
	name='fangwu_log',
  filename='D:\fangwu_log.ldf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
--tblUser --发布人信息表
--userId
--userName
--userTel
use fangwu
go

create table tblUser 
(
 userId int primary key identity(1,1),
 userName varchar,
 userTel  nvarchar(10)
)

--tblHouseType --房屋的类型
--typeId
--typName
create table tblHouseType 
(
 typeId  int primary key identity(1,1),
 typName varchar check(typName='别墅'or typName='普通住宅'or typName='平房' or typName='地下室') not null
)
--tblQx --区县
--qxId
--qxName
create table tblQx 
(
 qxId int primary key identity(1,1),
 qxName varchar check(qxName='百色'or qxName='贵港' or qxName='桂林') not null
)

--tblHouseInfo--房屋信息表
--id
--desc
--userId  --
--zj
--shi
--ting
--typeId --
--qxId --
create table tblHouseInfo
(
  id int primary key identity(1,1),
  userId int foreign key references tblUser (userId),
  zj money,
  shi varchar,
  ting int,
  typeId int foreign key references tblHouseType (typeId),
  qxId int foreign key references tblQx (qxId)
)








             