use master
create database bbs
on
(

name =bbs,
filename='D:\bbs.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)



log on 

(

    name='bbs_log',

	filename='D:\bbs_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=10MB

)
go
use bbs
go
--用户信息表
create table bbsusers
(
uid int identity(1,1) not null ,
unaem varchar(10),
usex varchar(2) not null ,
uage int not null ,
upoint int not null 
)

alter table bbsusers add constraint PK_UID  primary key (uid)
alter table bbsusers add constraint UN_ unique (unaem)
alter table bbsusers add constraint CK_ check(usex='男'or usex='女')
alter table bbsusers add constraint CK_ check(uage>=15 or uage<=60)
--板块表
create table bbssection
(sid int  identity(1,1),
snaeme varchar(10)not null ,
suid int  
)
alter table bbssection add constraint PK_sid primary key (sid)
alter table bbssection add constraint FK foreign key (suid) references bbsusers(uid)


--主贴表
create table bbstopic
(
tid int not null primary key identity(1,1),
tuid int foreign key references bbsusers(uid),
tsid int foreign key references bbssection(sid ),
ttitle varchar(100) not null,
tmsg text not null,
ttime datetime,
tcount int 
)
insert into bbstopic(tuid,tsid,ttitle,tmsg ,ttime ,tcount ) values(2,    0,'马巧晶','谁是马巧晶',2008-7-8,1),  
                                                                  (3,    4,'.NET  ',' 与JAVA的区别是什么呀？  ','2008-9-1 ','2'),
				                                                  (5,    6,'今年夏天最流行什么 ',' 有谁知道今年夏天最流行','2008-9-10','0')
			

--回帖表
create table bbsreply
(
rid int primary key identity(1,1),
ruid int foreign key references bbsusers(uid ),
rtid int foreign key references bbstopic(tid ),
rmsg text not null ,
rtime datetime 
)
insert into bbsReply(rUID,rTID ,rMsg,rTime) values(1,2,'二班长的最丑的就是范跑跑',0123),
                                                  (4,3,'我挂科的不知道这么深奥的问题',0123),
                                                  (5,6,'这个夏天最流行的当然是黑丝啊破洞的那种',0123)


insert into bbsusers(unaem,usex,uage,upoint) values('杜海彪','女',0,0)
insert into bbsusers(unaem,usex,uage,upoint) values('马巧晶','女',18,4)
insert into bbsusers(unaem,usex,uage,upoint) values('大一新生','男',19,2)

--备份

select unaem ,upoint into bbspoint from bbsusers

--开设
insert into bbssection(snaeme ,suid) values('技术交流','0'),( '读书世界', '0'),('生活百科' , '0'),('八卦区' ,'0')

 delete bbsusers where unaem ='杜海彪'
 --增加积分
 update bbsusers set upoint='10' where unaem='马巧晶'
 select * from  bbssection
delete bbssection where snaeme='生活百科' --删表回贴表
 delete bbsreply
 truncate
                                     