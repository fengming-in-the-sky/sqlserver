create database TestDB
on(
	name='TestDB',
	filename='E:\TestDB.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log 
on(
	name='TestDB_log',
	filename='E:\TestDB_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
use TestDB 
create table typelnfo
(
	typeId int primary key identity,
	typeName varchar(10) not null,
)
create table loginlnfo
(
	LoginId int primary key identity,
	LoginName varchar(10) not null unique,
	LoginPwd varchar(20) not null default('123456'),
	LoginSex char not null,
	LoginBrithd datetime not null,
	LoginVip varchar(10) not null  
)








create database company
on
(
	name='company',
	filename='E:\company.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='company_log',
	filename='E:\company_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use company 
go

create table sectionlnfo
(
	sectionID int identity primary key,
	sectionName varchar(10) not null
)
create table userlnfo
(
	userNo int identity primary key not null,
	userName varchar(10) unique not null check (len (userName)>4),
	userSex varchar(2) not null check(userSex='��' or userSex='Ů'),
	userAge int not null check(userAge>=1 or userAge<=100),
	userAddress varchar(50) default('����'),
	userSection int references sectionlnfo(sectionID)
)
create table worklnfo
(
	workld int identity primary key not null,
	userld int references userlnfo(userNo) not null,
	workTime datetime not null,
	workDescription varchar(40) not null check(workDescription='�ٵ�' or workDescription='����' or workDescription='��' or workDescription='����' or workDescription='�¼�' )
)








create database School
on(
	name='School',
	filename='E:\School.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='School_log',
	filename='E:\School_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
create table banji
(
	classid int primary key identity,
	classN1 varchar(10) not null unique,
	classKB datetime not null,
	classMS text not null
)
create table xuesheng
(
	sdtXH int primary key identity,
	sdtXM varchar(10) check(len (sdtXM)>2) unique,
	sdtXB  varchar(2) default('��') not null check(sdtXB=('��')or sdtXB=('Ů')),
	sdtNL int check(sdtNL>=15 or sdtNL<=40) not null,
	sdtZZ varchar(30)  default('�����人'),
	classid int references banji(classid),
)
create table kecheng
(
	bianhao int primary key identity,
	kechengming varchar(20) not null unique,
	kechengmiaoshu text not null
)
create table chengji
(
	chengjiID int primary key identity,
	sdtXH int references xuesheng(sdtXH) not null,
	bianhao int references kecheng(bianhao) not null,
	chengjiqujian int check(chengjiqujian>=0 or chengjiqujian<=100)
)








create database fangwu
on
(
	name='fangwu',
	filename='E:\fangwu.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='fangwu_log',
	filename='E:\fangwu_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
create table tbIUser
(
	userID int primary key identity,
	userName varchar,
	userTel nvarchar(10)
)
create table tbIHouseType
(
	typeID int primary key identity,
	typName varchar check(typName='����' or typName='��ͨ����' or typName='ƽ��' or typName='������') not null,
)
create table tbIQx
(
	qxID int primary key identity,
	qxname varchar check(qxName='���' or qxName='����' or qxName='����') not null,
)
create table tbIhouseInfo
(
	id int primary key identity,
	userID int foreign key references tbIUser(userID),
	zj money,
	shi varchar,
	ting int ,
	typeID int foreign key references tbIHouseType(typeID),
	qxID int foreign key references tbIQx(qxId)
)