--1
use master
go

create database TestDB
on
(
	name = TestDB,
	filename = 'D:\数据库\TestDB.mdf'
)
log on
(
	name = TestDB_log,
	filename = 'D:\数据库\TestDB_log.ldf'
)

use  TestDB

create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName nvarchar(20) not null
)

create table loginInfo
(
	LoginId int primary key identity,
	LoginName nvarchar(10) not null unique,
	LoginPwd nvarchar(20) not null default(123456),
	LoginSex nvarchar(1) check(LoginSex in ('男','女')),
	LoginBirDay date,
	LoginSort nvarchar(10)
)


--2
use master 
go

create database company
on
(
	name = 'company',
	filename = 'D:\数据库\company.mdf',
	size = 5,
	maxsize = 50,
	filegrowth = 10%
)
log on
(
	name = 'company_log',
	filename = 'D:\数据库\company_log.ldf'
)

use company

create table sectionInfo
(
	sectionID int primary key identity,
	sectionName varchar(10) not null
)

create table userInfo
(
	userNo int identity primary key not null,
	userName varchar(10) unique not null check(len(userName)>4) ,
	userSex varchar(2) not null check(userSex in ('男','女')),
	userAge int not null check(userAge>1 and userAge<=100),
	userAddress varchar(50) default('湖北'),
	userSection int foreign key	references sectionInfo(sectionID)
)

create table workInfo
(
	workId int identity primary key not null,
	userId int foreign key references userInfo(userNo),
	workTime datetime not null,
	workDescription varchar(40) not null check(workDescription in ('迟到','早退','旷工','病假','事假'))
)


--3
use master
go 

create database class
on
(
	name = 'class',
	filename = 'D:\数据库\class.mdf'
)

log on
(
	name = 'class_log',
	filename = 'D:\数据库\class_log.ldf'
)

create table classInfo
(
	classID  varchar(10) not null unique,
	classStart datetime not null,
	classdescription text
)

create table student
(
	ID int primary key identity,
	Name nvarchar(20) check(len(Name)>2) unique,
	Sex nvarchar(1) check(Sex in ('男','女')) not null,
	 Age int check(Age>15),
	 Home nvarchar(10) default('湖北武汉'),
	 classID int references classInfo(classID)
)

create table course
(
	course int primary key identity,
	subjects nvarchar(10) not null unique,
	describe text
)

create table resullt
(
	resulltID int primary key identity ,
	ID int not null  ,
	courseID int not null references course(course),
	report int check(report>=0 and report<= 100)
)

--4
use master
go

create database house
on
(
	name = 'house',
	filename = 'D:\数据库\house.mdf'
)

log on
(
	name = 'house_log',
	filename = 'D:\数据库\house_log.ldf'
)

use house

create table tbUser
(
	userId int identity(1,1) primary key ,
	useName nvarchar(15) not null,
	userTel varchar(11) not null
)

create table tblHouseType
(
	typeId int primary key ,
	typName nvarchar(10) check(typName in ('别墅','普通住宅','平房','地下室')),
)

create table tblQx
(
	qxId int primary key ,
	qxName nvarchar(2) check(qxName in ('武昌','汉阳','汉口'))
)

create table tblHouseInfo
(
	id int ,
	userid int references tbUser(userid),
	zj int not null ,
	shi int not null,
	ting int not null,
	typeld int references tblHouseType(typeId),
	qxId int references tblQx(qxId)
)

