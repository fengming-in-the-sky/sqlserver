use master
go
create database stumanagement
go
use stumanagement
go
create table classmanagement
(
classid int primary key identity(1,1),
stuname nvarchar(5) not null unique,
classtime datetime not null,
classdescribe text

)
create table stumessage
(
stuid int primary key identity(1,1),
stuname nvarchar check(len(stuname)>2) not null,
stusex nvarchar(1) check(stusex='��' or stusex='Ů') default('��'),
stuage int check(stuage>=15 or stuage<=40),
stuaddress text default('�����人')
)
create table coursemessage
(
courseid int primary key identity(1,1),
coursename nvarchar(5)not null,
describe text
)