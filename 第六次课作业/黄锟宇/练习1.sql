﻿use master
go
--先创建一个数据库，数据库名为TestDB，要求有一个数据文件，一个日志文件，注意命名规范，文件存放在D盘下
create database TestDB
on
(
	name=TestDB,
	filename='D:\SIX1\TestDB.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%
)
log on
(
	name=TestDB_ldf,
	filename='D:\SIX1\TestDB.ldf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10%
)

use TestDB
go
--会员类别表(typeInfo)：
create table typelnfo
(
	--类别编号(typeId)：主键、自动编号
	typeld int primary key identity(1,1),
	--类别名(typeName): varchar(10)  不能为空
	typeName varchar(10) not null,
)
--登录用户表(loginInfo)：
create table loginInfo
(
	--编号(LoginId)，数据类型(int)，主键、自动编号
	LoginId int primary key identity(1,1),
	--账户(LoginName)，文本，长度为10，非空，必须唯一，不能重复
	LoginName nvarchar(10) not null unique ,
	--密码(LoginPwd)，文本，长度为20，非空、默认值为‘123456
	LoginPwd varchar(20) default(123456),
	-- 性别(自定类型)
	LoginSex nvarchar(1) check(LoginSex='男' or LoginSex='女'),
	--生日(自定类型)
	LoginBir date ,
	--会员类别(自定类型)
	LoginSort text not null
)
--. 先创建一个数据库用来存放某公司的员工信息，数据库的名称为company，
--包含2个数据文件1个日志文件，数据文件和日志文件全部存放在D盘中，初始大小，增长和最大大小自己设定
create database Company
on(
	name=Company,
	filename='D:\SIX1\Company.mdf',
	size=5MB,
	maxsize=5MB,
	filegrowth=10%
)
 log on(
	name=Company_ldf,
	filename='D:\SIX1\Company.ldf',
	size=5MB,
	maxsize=5MB,
	filegrowth=10%
)

use Company
go
--再创建表：部门信息表（sectionInfo）
create table SectionInfo
(
	--部门编号  sectionID  int 标识列  主键
	SectionID int primary key identity(1,1) not null,
	--部门名称  sectionName  varchar(10)  不能为空
	SectionNamme varchar(10) not null,
)
--员工信息表（userInfo）
create table UserInfo
(
	--员工编号  userNo  int  标识列 主键  不允许为空
	UserNo int identity(1,1) primary key not null,
	--员工姓名  userName  varchar(10)  唯一约束 不允许为空 长度必须大于4
	UserName varchar(10) unique not null check(len(UserName)>=4),
	--员工性别  userSex   varchar(2)  不允许为空  只能是男或女
	UserSex varchar not null check(UserSex='男' or Usersex='女'),
	--员工年龄  userAge   int  不能为空  范围在1-100之间
	UserAge int not null check(UserAge>=1 or UserAge<=100),
	--员工地址  userAddress  varchar(50)  默认值为“湖北”
	UserAddress varchar(50) default('湖北'),
	--员工部门  userSection  int  外键，引用部门信息表的部门编号
	UserSection int foreign key  references SectionInfo(SectionID),
)
--员工考勤表（workInfo）
create table WorkInfo
(
	--考勤编号  workId int 标识列  主键  不能为空
	workld int identity(1,1) primary key not null,
	--考勤员工  userId  int 外键 引用员工信息表的员工编号 不能为空
	userId int foreign key references UserInfo(UserNo),
	--考勤时间  workTime datetime 不能为空
	WorkTime datetime Not null ,
	--考勤说明  workDescription  varchar(40) 不能为空 
	--内容只能是“迟到”，“早退”，“旷工”，“病假”，“事假”中的一种
	WorkDescription varchar(40) not null 
	check(WorkDescription='迟到' or WorkDescription='早退' or WorkDescription='旷工' or WorkDescription='事假' or WorkDescription='¼')
)
--为学校开发一个学生管理系统，请为该系统创建数据库，主要存放的信息有：班级信息、学生信息、课程信息、学生考试成绩
create database AdminiInfo
go
                
use AdminiInfo
go
--班级信息
create table ClassInfo
(
	--班级编号 classid (主键、标识列)
	ClassID int primary key identity(1,1),
	--班级名称(例如：T1、T2、D09等等):不能为空，不能重复
	ClassName nvarchar(10) not null unique,
	--开办时间：不能为空
	ClassTime  time not null,
	-- 班级描述
	ClassDes text
)
--学生信息
create table StudentInfo
(
	--学号：主键、标识列
	StuID int primary key identity(1,1),
	--姓名：长度大于2，不能重复
	StuName nvarchar(10) not null unique check(len(StuName)>2),
	--性别：只能是‘男’或‘女’，默认为男，不能为空
	StuSex nvarchar(2) not null  default('男') check(StuSex='' or StuSex='女'),
	--年龄：在15-40之间，不能为空
	StuAge int not null,
	--家庭地址：默认为“湖北武汉”
	StuAdress varchar(12)  default('湖北武汉'),
	--所在的班级编号
	ClassID int  foreign key references ClassInfo(ClassID)
)
--课程信息：编号：主键、标识列
create table CourseInfo
(
	--编号：主键、标识列
	CourseID int primary key identity(1,1),
	--课程名：不能为空，不能重复
	CourseName nvarchar(10) not null unique,
	-- 课程描述：
	CourseDes text
)
--成绩信息：
create table Score
(
	--成绩编号：主键、标识列
	ScoreID int primary key identity(1,1),
	--成绩所属于的学生编号，不能为空
	ScoreStuID int foreign key references StudentInfo(StuID),
	--成绩所属的课程编号，不能为空
	ScoreClassID int foreign key references ClassInfo(ClassID),
	--成绩：在0-100之间
	Score int check(Score>=1 and Score<=100) not null
)
--为一个房屋出租系统创建一个数据库，数据库中主要存放房屋的信息
create database HomeInfo
go

use HomeInfo
go
--tblUser --发布人信息表
create table TbIUser
(
	--发布人的信息(姓名和联系电话)
	UserID int primary key identity(1,1),
	UserName nvarchar(10) not null,
	UserTel nvarchar(11) not null check(len(UserTel)=11)
)
--tblHouseType --房屋的类型
create table tblHouseType
(
	TypelD int primary key identity(1,1),
	Typname nvarchar(10) not null check(Typname='' or Typname='ƽ' or Typname='ͨסլ' or Typname='')
)

create table TbIQx
(
	QxID int primary key identity(1,1),
	Qxname nvarchar(10) not null check(Qxname='' or Qxname='' or Qxname='')
)
--房屋所属的区县(武昌、汉阳、汉口)，
create table TblHouseInfo
(
	HomeID int primary key identity(1,1),
	HomeDesc nvarchar(10),
	UserID int foreign key references  TbIUser(UserID),
	Homezj int not null,
	HomeShi int not null,
	HomeTing int not null,
	TypeID int foreign key references tblHouseType(TypelD),
	QxID int foreign key references TbIQx(Qxid)
)


