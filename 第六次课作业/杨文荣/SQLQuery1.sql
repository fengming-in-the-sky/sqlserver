use master
go

create database TestDB
on
(
	name='TestDB',
	filename='D:\TestDB.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='TestDB_log.ldf',
	filename='D:\TestDB_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use TestDB
go 
create table typeInfo
(
	typeld int primary key identity(1,1),
	typeName varchar(10) not null
)

create table loginInfo
(
	Loginld int primary key identity(1,1),
	LoginName nvarchar(10) not null unique,
	LoginPwd nchar(20) not null default('123456'),
	Sex nvarchar(1) check(Sex='Ů'or Sex='��'),
	Birthday nvarchar(10),
	MemType nvarchar(5)
)
go
use master
go
create database company
on
(
	name='company',
	filename='D:\company.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='company_log',
	filename='D:\company_log.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use company
go
create table sectionInfo
(
	sectionID int primary key identity(1,1),
	sectionName varchar(10) not null
)

create table userInfo
(
	userNo int primary key identity(1,1) not null,
	userName varchar(10) unique not null check(len(userName)>4),
	userSex varchar(2) not null check(userSex='��'or userSex='Ů'),
	userAge int not null check(userAge>=1 and userAge <=100),
	userAddress varchar(50) default('����'),
	userSection int constraint FK_userInfo_userSection references sectionInfo(sectionID)
)

create table workInfo
(
	workld int identity(1,1) primary key not null,
	userld int constraint FK_workInfo_userld references userInfo(userNo),
	workTime datetime not null,
	workDescription varchar(40) not null check (workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�') 
)
go
use master
go
create database Student
on
(
	name='Student',
	filename='D:\student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use Student
go
create table classInfo
(
	classID int primary key identity(1,1),
	className varchar(20) not null unique,
	openTime varchar(10) not null,
	classDescription text
)

create table studentInfo
(
	stuID int primary key not null,
	Sex varchar(2) default('��') check(Sex='��' or Sex='Ů'),
	stuAge int check(stuAge>=15 or stuAge<=40) not null,
	stuAddress nvarchar(20) default('�����人'),
	classID int constraint FK_studentInfo_classID references classInfo(classID)
)
create table courseInfo
(
	courseID int primary key identity(1,1),
	courseName nvarchar(10) not null unique,
	courseDescription text
)
create table scoreInfo
(
	scoreID int primary key identity(1,1),
	stuID int constraint FK_scoreInfo_stuID references studentInfo(stuID),
	courseID int constraint FK_scoreInfo_courseID references courseInfo(courseID),
	Score int check(Score>=0 and Score<=100)
)
go
use master
go
create database House
on
(
	name='House',
	filename='D:\House.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='House_log',
	filename='D:\House_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use House
go
create table tblUser
(	
	userID int primary key identity(1,1),
	userName nvarchar(5) not null,
	userTel char(11) not null
)
create table tblHouseType
(
	typeID int primary key identity(1,1),
	typName nvarchar(20) not null
)
create table tblQx
(
	qxID int primary key identity(1,1),
	qxName nvarchar(20) not null 
)
create table tblHouseInfo
(	
	id int primary key identity(1,1),
	userID int constraint FK_tblHouseInfo_userID references tblUser(userID),
	zj int not null,
	shi int not null,
	ting int not null,
	typeID int constraint FK_tblHouseInfo_typeID references tblHouseType(typeID),
	qxID int constraint FK_tblHouseInfo_qxID references tblQx(qxID)
)