use master
go
create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use bbs
go

create table bbsUsers
(
	UID int identity(1,1),
	uName varchar(10) not null,
	uSex varchar(2) not null,
	uAge int not null,
	uPoint int not null,
)

alter table bbsUsers add constraint PK_bbsUsers_UID primary key (UID)
alter table bbsUsers add constraint UQ_bbsUsers_uName unique (uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex='男'or uSex='女')
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_upoint check(uPoint>=0)

create table bbsSection
(
	sID int identity(1,1),
	sName varchar(10) not null,
	sUid int,
)
alter table bbsSection add constraint PK_bbsSection_sID primary key (sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key (sUid) references bbsUsers(UID)

create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int constraint FK_bbsTopic_tUID references bbsUsers(UID),
	tSID int constraint FK_bbsTopic_tSID references bbsSection(sID),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)
create table bbsReply
(
	rID int primary key identity(1,1),
	rUID int constraint FK_bbsReply_rUID references bbsUsers(UID),
	rTID int constraint FK_bbsReply_rTID references bbsTopic(tID),
	rMsg text not null,
	rTime datetime
)

insert into bbsUsers
select '小雨点','女','20', '0'union
select'逍遥', '男',  '18',  '4'union
select	'七年级生','男','19', '2'
select uName,uPoint into bbsPoint from bbsUsers

insert into bbsSection
select '技术交流',1 union
select '读书世界',3 union
select '生活百科',1 union
select' 八卦区',3 

insert into bbsTopic
select 2,4,'范跑跑','谁是范跑跑',' 2008-7-8',1 union
select 3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2 union
select 1,3,'今年夏天最流行什么',' 有谁知道今年夏天最流行什么呀？',' 2008-9-10',0
insert into bbsReply
select 1,1,'随便','2020-12-01' union
select 2,2,'随便','2020-12-01' union
select 3,3,'随便','2020-12-01'

alter table bbsTopic drop FK_bbsTopic_tUID
alter table bbsReply drop FK_bbsReply_rUID
alter table bbsSection drop FK_bbsSection_sUid
delete bbsUsers where uName='逍遥'

update bbsUsers set uPoint=10 where uName='小雨点'

alter table bbsTopic drop FK_bbsTopic_tSID 
delete bbsSection where sName='生活百科'

delete bbsReply

