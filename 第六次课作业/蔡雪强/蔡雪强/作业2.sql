 use master
go
create database bbs
on
(
name='bbs',
filename='D:\SQL\bbs.mdf'
)
log on
(
name='bbs_log',
filename='D:\SQL\bbs_log.ldf'
)
use bbs
go
create table bbsUsers
(
UID int primary key identity(1,1),
uName varchar(10) not null,
uSex varchar(2) not null check(uSex='男' or uSex='女'),
uAge int not null check(uAge>=15 and uAge<=40),
uPoint int not null check(uPoint>=0)
)
create table bbsTopic
(
tID int primary key identity(1,1),
tUID int,
tSID int,
tTitle varchar(100) not null,
tMsg text not null,
tTime datetime,
tCount int
)
create table bbsReply
(
rID int primary key identity(1,1),
rUID int constraint FK_bbsUsers_UID references bbsUsers(UID),
rTID int constraint FK_bbsTopic_tID references bbsTopic(tID),
rMsg text not null,
rTime datetime
)
create table bbsSection
(
sID int primary key identity(1,1),
sName varchar(10) not null,
sUid int,
)
alter table bbsTopic add constraint FK_bbsUsers1_UID foreign key(tUID) references bbsUsers(UID)
alter table bbsTopic add constraint FK_bbsSection_sID foreign key(tSID) references bbsSection(sID)
alter table bbsSection add constraint FK_bbsUsers2_UID foreign key(sUid) references bbsUsers(UID)
insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection(sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2,4,'范跑跑','谁是范跑跑','2008-7-8',1),(3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀','2008-9-10',0)
insert into bbsReply(rMsg,rTime,rUID,rTID) values('666','2008-1-1',1,2),('666','2008-1-1',2,3),('666','2008-1-1',3,1)
alter table bbsTopic drop constraint FK_bbsUsers1_UID
alter table bbsReply drop constraint FK_bbsUsers_UID
delete from bbsUsers where uID=2
insert into bbsUsers(uName,uPoint) values('小雨点',10)
alter table bbsTopic drop constraint FK_bbsSection_sID
delete from bbsSection where sName='生活百科'
delete from bbsReply