create database bbs
go
use bbs
go

--用户信息表
create table bbsUser
(
 UID int  identity(1,1),
 uName varchar(10) not null,
 uSex  varchar(2) not null check(uSex='男'or uSex='女'),
 uAge  int not null check(uAge>=15 and uAge<=60),
 uPoint int not null check(uPoint>=0),
)
--添加约束
--主键约束
alter table bbsUser add constraint PK_bbsUser_UID primary key (UID)
--唯一约束
alter table bbsUser add constraint UK_bbsUser_uName unique(uName)

--版块表
create table bbsSection
(
 sID  int identity(1,1) primary key,
 sName  varchar(10) not null,
 sUid int,
)
--外键 引用用户信息表的用户编号
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(UID)

--主贴表
create table bbsTopic
(
 tID int primary key identity(1,1),
 tUID int ,
 tSID  int ,
 tTitle varchar(100) not null,
 tMsg text not null,
 tTime  datetime,
 tCount int,
)
--添加约束
--外键约束 引用用户信息表的用户编号
alter table bbsTopic add constraint FK_bbsTopic_tUID foreign key(tUID) references bbsUser(UID)
--外键约束 引用板块表的版块编号
alter table bbsTopic add constraint FK foreign key (tSID) references bbsSection(sID)

--回贴表
create table bbsReply
(
 rID int primary key identity(1,1),
 rUID int,
 rTID  int,
 rMsg  text not null,
 rTime  datetime,
)
--外键约束 引用用户信息表的用户编号
alter table bbsReply add constraint FK_bbsReply_rUID foreign key (rUID) references bbsUser(UID)

--外键约束 引用主贴表的主贴编号
alter table bbsReply add constraint FK_bbsReply_rTID foreign key (rTID) references bbsTopic(tID)

--现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--小雨点  女  20  0
--逍遥    男  18  4
--七年级生  男  19  2

insert into bbsUser(uName,uSex,uAge,uPoint) values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级','男',19,2)

--2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
--插入多行：
select uName,uPoint into bbsPoint from bbsUsers

--给论坛开设4个板块
--名称        版主名
--技术交流    小雨点
--读书世界    七年级生
--生活百科     小雨点
--八卦区       七年级生

insert into bbsSection(sName,sUid)  values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

--4.向主贴和回帖表中添加几条记录
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2,4,'范跑跑','谁是范跑跑',2008-7-8,1) ,(3,2,'.NET','与Java的区别是什么?',2008-9-1,2),(1,4,'今年夏天流行什么','有谁知道今年夏天最流行什么?',2008-9-10,0)
insert into bbsReply(rMsg,rTime) values('逃跑的老师',2008-7-8),('？？',2008-7-9),('流行看美女',2008-7-10)
select * from bbsUser 
--5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，
--请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
delete from bbsTopic where tUID=2
delete from bbsReply where rUID=2
delete from bbsReply where rID=1
delete from bbsUsers where uName='逍遥'

--6.因为小雨点发帖较多，将其积分增加10分
update bbsUsers  set upoint=30 where uName='小雨点'
--7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
delete from bbsTopic where tSID=3
delete from bbsSection where sName='生活百科'

--	8.因回帖积累太多，现需要将所有的回帖删除
delete from bbsReply