create database TestDB
go
use TestDB
go
create table typeInfo
(
 typeId int primary key identity(1,1),
 typeName nvarchar(10) not null,
)
create table loginInfo
(
 LoginId int primary key identity(1,1),
 LoginName text not null unique,
 LoginPwd text not null default('1,2,3,4,5,6'),
 LoginSex char(2),
 LoginSr time,
 LoginHy char,
)
create table sectionInfo
(
 sectionID  int identity(1,1) primary key,
 sectionName  varchar(10) not null,
)
create table userInfo
(
 userNo int identity(1,1) primary key not null,
 userName varchar(10) unique not null,
 userSex  varchar(2) not null check(userSex='��'or userSex='Ů'),
 userAge  int not null  check(userAge>=1 and userAge<=100),
 userAddress  varchar(50) default('����'),
 userSection  int foreign key(userSection) references sectionInfo(sectionID) ,
)
create table workInfo
(
 workId int identity(1,1) primary key not null,
 userId int foreign key(userId) references userInfo(userNo) not null,
 workTime datetime not null,
 workDescription  varchar(40) not null check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�'),
)
create database StudenInfo
go
use StudenInfo
go
create table Classinfo
(
 classid int primary key identity(1,1),
 classname int not null unique,
 classtime datetime not null,
 classms text not null,
)
create table Stuinfo
(
 StuId int primary key identity(1,1),
 Stuname int check(Stuname>2) not null,
 Stusex char(2) default('��') check(Stusex='��'or Stusex='Ů') not null,
 Stuaeg int check(Stuaeg>=15 and Stuaeg<=40) not null,
 StuDiZhi varchar default('�����人'),
)
create table kecheng
(
 KCid int primary key identity(1,1),
 KCname nchar not null unique,
 KCms text,
)
create table CJinfo
(
 CJid int primary key identity (1,1),
 XSid int not null,
 KCid int not null,
 Cj int check(CJ>=0 and CJ<=100),
)
create table tblUser 
(
 userId int primary key identity (1,1),
 userName varchar not null,
 userTel int not null,
)
create table tblHouseType
(
 typeId int not null,
 typName varchar not null,
 tblQx varchar not null,
 qxId int not null,
 qxName varchar,
)
create table tblHouseInfo
(
 id int primary key identity(1,1),
)