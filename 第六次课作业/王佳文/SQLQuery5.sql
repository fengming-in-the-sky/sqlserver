use master
go 
create database tbl
on(
	name='tbl',
	filename='C:\TEXT\tbl.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
log on(
	name='tbl_log',
	filename='C:\TEXT\tbl_log.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
go
use tbl
go
create table tblUser
(
	userId int primary key identity(1,1),
	userName nvarchar(10) not null,
	userTel varchar(11)  unique not null,
)
create table tblHouseType
(
	typeId int primary key identity(1,1),
	typName nvarchar(10) not null,
	tblQx  nvarchar(10) not null,
	qxId int  not null,
	qxName nvarchar(10) not null,
)
create table tblHouseInfo
(
	id int  primary key identity(1,1), 
	userId  int references tblUser(userId),
	typeId int references tblHouseType(typeId),
	qxId int  not null,
)
