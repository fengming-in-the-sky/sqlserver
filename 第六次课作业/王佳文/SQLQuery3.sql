use master
go 
create database students
on(
	name='students',
	filename='C:\TEXT\students.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
log on(
	name='students_log',
	filename='C:\TEXT\students_log.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
go
use Students
go
create table class
(
	 classid int primary key identity(1,1),
	 className nvarchar(10) unique not null,
	 classTime datetime not null,
	 classText text not null,
)
create table student
(
	stuID int primary key identity(1,1),
	stuName nvarchar(10) unique not null,
	stuSex nvarchar(2) default('��') check(stuSex='��' or stuSex='Ů'),
	stuAge int check(stuAge>=14 or stuAge<=40)not null,
	stuAddress nvarchar(20) default ('�����人') not null,
	classid int references class(classid),
)
 create table course
 (
	courseid int primary key identity(1,1),
	courseName nvarchar(10) unique not null,
	courseText text not null,
 )
 create table score
 (
	scoreID int primary key identity(1,1),
	stuID int references student(stuID) not null,
	courseid int references course(courseid) not null,
	score int check(score>=1 or score<=100)
 )
