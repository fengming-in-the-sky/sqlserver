use master
go

create database TestDB
on
(
    name='TestDB',
	filename='E:\TestDB.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
    name='TestDB_log',
	filename='E:\TestDB_log.ldf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use TestDB
go
create table typeInfo
(
    typeld int primary key identity(1,1),
	typeName varchar(10) not null
)

create table loginInfo
(   
    LoginId int primary key identity(1,1),
	LoginName nvarchar(10) not null unique ,
	LoginPwd nvarchar(20) not null default('123456'),
	LoginSex char(2) default('��') check(LoginSex='��' or LoginSex='Ů'),
	LoginBirthday date,
	LoginHuiyuan nvarchar(20)
)
go
use master
go

create database company
on
(
    name='company',
	filename='E:\company.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
    name='company_log',
	filename='E:\company_log.ldf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use company
go
create table sectionInfo
(
    sectionID  int primary key identity(1,1),
	ectionName  varchar(10) not null
)

create table userInfo
(
    userNo  int  primary key identity(1,1) not null,
	userName  varchar(10) unique check(userName>4) not null,
	userSex   varchar(2) not null check(userSex='��' or  userSex='Ů'),
    userAge   int  not null check(userAge>=1 and userAge<=100),
	userAddress  varchar(50) default('����'),
	userSection  int references sectionInfo(sectionID)
)
create table workInfo
(
    workId int primary key identity(1,1) not null,
	userId  int references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription  varchar(40) not null check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�') 
)
go
use master
go
create  database Studen01
go

use Studen01
go

create table classInfo
(
   classID int primary key identity(1,1),
   className nvarchar(20) not null unique,
   Opentime datetime not null,
   classDescription text
)

create table StuInfo
(
   stuID int primary key identity (1,1),
   stuName nvarchar(10) check(stuName>2) unique,
   stuSex char(2) default('��') check(stuSex='��' or stuSex='Ů') not null,
   stuAag int check(stuAag>=15 and stuAag<=40) not null,
   stuAddress nvarchar(200) default('�����人')
)

create table CourInfo
( 
   CourID int primary key identity(1,1),
   CourName nvarchar(10) not null unique,
   CourDescription text
)

create table PerInfo
(
   PerNO int primary key identity(1,1),
   stuID int references StuInfo(stuID) not null,
   CourID int references CourInfo(CourID) not null,
   Perscore int check(Perscore>=0 and Perscore<=100)
)
go

use master
go

create database Rentalhousing
go
use Rentalhousing
go

create table tblUser
(
    userId int primary key identity(1,1),
	userName nvarchar(20) not null unique,
	userTel varchar(20) not null 
)

create table tblHouseType
(
   typeId int primary key identity(1,1),
   typName nvarchar(20) not null unique
)

create table tblQx
(
   qxID int primary key identity(1,1),
   qxName nvarchar(20) not null unique
)

create table tblHouseInfo
(
  id int primary key identity(1,1),
  descv nvarchar(20) not null,
  userld nvarchar(20) not null,
  zj nvarchar(20) not null,
  ting nvarchar(20) not null,
  typeId int references tblHouseType(typeId),
  qxId int references tblQx(qxId)
)