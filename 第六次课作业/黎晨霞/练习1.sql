use master
go

create database bbs
on
(
   name='bbs',
   filename='D:\bbs.mdf',
   size=10mb,
   maxsize=100mb,
   filegrowth=10mb
)
log on
(
   name='bbs_log',
   filename='D:\bbs_log.ldf',
   size=10mb,
   maxsize=100mb,
   filegrowth=10mb
)
go

use bbs
go

create table bbsUsers
(
    UID int not null,
	uName varchar(10) not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null 
)
go
use bbs
go
select * from bbsUsers
alter table bbsUsers add constraint PK primary key  (UID)
alter table bbsUsers add constraint FK unique (uName)
alter table bbsUsers add constraint CK check(uSex='男' or uSex='女') 
alter table bbsUsers add constraint DK check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint UK check(uPoint>=0)

create table bbsSection
(
     sID  int not null,
	 sName  varchar(10) not null,
	 sUid   int not null
)
go
use bbs
go

alter table bbsSection add constraint BK primary key(sID)
alter table bbsSection add constraint NK foreign key (sUid) references bbsUsers( UID)

go
use bbs
go
create table bbsTopic
(
     tID  int primary key identity(1,1),
	 tUID  int references bbsUsers(UID),
     tSID  int references bbsSection(sID),
	 tTitle  varchar(100) not null,
	 tMsg  text not null,
	 tTime  datetime,
     tCount  int
)
create table bbsReply
(
    rID int primary key identity(1,1),
    rUID  int references bbsUsers(UID),
	rTID  int references bbsTopic(tID),
	rMsg  text not null,
	rTime  datetime
)

insert into bbsUsers(UID,uName,uSex,uAge,uPoint)
values(1,'小雨点','女','20','0')
insert into bbsUsers(UID,uName,uSex,uAge,uPoint)
values(2,'逍遥','男','18','4')
insert into bbsUsers(UID,uName,uSex,uAge,uPoint)
values(3,'七年级生','男','19','2')

go
use bbs
go

select uName,uPoint into bbsPoint from bbsUsers

insert into bbsSection(sID,sName,sUid)
values(5,'技术交流',1)
insert into bbsSection(sID,sName,sUid)
values(6,'读书世界',3)
insert into bbsSection(sID,sName,sUid)
values(7,'生活百科',1)
insert into bbsSection(sID,sName,sUid)
values(8,'八卦区',3)

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values(2,8,'范跑跑','谁是范跑跑','2008-7-8',1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values(3,5,'.NET','与JAVA的区别是什么呀？','2008-9-1',2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)
values(1,7,'今年夏天最流行什么 ','有谁知道今年夏天最流行什么呀？','2008-9-10',0)

insert into bbsReply(rUID,rTID,rMsg,rTime)
values(3,2,'范跑跑','2008-7-8')

select * from bbsUsers
delete  bbsUsers where  uName ='逍遥'
alter table bbsSection drop NK
alter table bbsTopic drop FK__bbsTopic__tUID__1920BF5C
update bbsUsers set uPoint ='10' where uName='小雨点'

select * from bbsSection
delete bbsSection where sName='生活百科'

select * from bbsReply
delete bbsReply