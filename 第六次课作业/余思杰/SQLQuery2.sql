use master
go
create database bbs
on
(
	name='bbs',
	filename='D:\SQL作业\SQL作业6\2\bbs.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)

log on
(
	name='bbs_log',
	filename='D:\SQL作业\SQL作业6\2\bbs_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use bbs
create table bbsUsers
(
	UID int identity(1,1),
	uName varchar(10) not null,
	uSex varchar(2) not null,
    uAge int not null,
	uPoint int not null
)
go

alter table bbsUsers add constraint PK_bbsUsers_UID primary key (UID)
alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName) 
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex='男' or uSex='女')
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>=0)


use bbs
create table bbsSection
(
	sID int identity(1,1),
	sName varchar(10) not null,
	sUid int
)
go

alter table bbsSection add constraint PK_bbsSection_sID primary key (sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(UID)

use bbs
create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int constraint FK_bbsUsers_UID references bbsUsers(UID),
	 tSID int constraint FK_bbsSection_sID references bbsSection(sID),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)
go

use bbs
create table bbsReply
(
	rID int primary key identity(1,1),
    rUID int constraint FK_bbsReply_rUID references bbsUsers(UID),
	rTID int constraint FK_bbsTopic_tID references bbsTopic(tID),
	rMsg text not null,
	rTime datetime 
)
go


--1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
--2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
select uName,uPoint into bbsPoint  from bbsUsers
--3.给论坛开设4个板块
--	  名称        版主名
--	  技术交流    小雨点
--	  读书世界    七年级生
--	  生活百科     小雨点
--	  八卦区       七年级生
insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

select * from bbsUsers
--4.向主贴和回帖表中添加几条记录
	  
--	   主贴：

--	  发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
--	  逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
--	  七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
--	  小雨点   生活百科    今年夏天最流行什么     有谁知道今年夏天最流行  2008-9-10  0
--						      什么呀？
	   
--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values
(2,2,'范跑跑','谁是范跑跑','2008-7-8',1),(3,3,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),
(1,4,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10',0)
--5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
alter table bbsTopic drop constraint FK_bbsUsers_UID
delete from bbsUsers where uName='逍遥'
--6.因为小雨点发帖较多，将其积分增加10分
update bbsUsers set uPoint=10 where uName='小雨点'
--7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
alter table bbsTopic drop constraint FK_bbsSection_sID
delete from bbsSection where sName='生活百科'
--8.因回帖积累太多，现需要将所有的回帖删除
delete from bbsReply


select * from bbsTopic
select * from bbsSection
select * from bbsUsers
select * from bbsReply
