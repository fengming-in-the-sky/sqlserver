use master
go
create database TestDB
on
(
	name='TestDB',
	filename='D:\TestDB.mdf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='TestDB_log',
	filename='D:\TestDB_log.ldf',
	size=5,
	maxsize=100,
	filegrowth=10%
)
go
use TestDB
go
create table typeInfo
(
	typelf int primary key identity(1,1),
	typeName varchar(10) not null,
)
create table loginInfo
(
	Loginld int primary key identity(1,1),
	LoginName char(10) not null unique,
	LoginPwd char(20) not null default(123456),
	loginSex char(1) check(loginSex= '��' or loginSex= 'Ů') default('��'),
	Birthday datetime not null,
	Member int not null
)
use master
go
create database company
on
(
	name ='company',
	filename='D:\company.mdf',
	size =5,
	maxsize =100,
	filegrowth=10%
)
log on
(
	name ='company_log',
	filename='D:\company_log.ldf',
	size =5,
	maxsize =100,
	filegrowth=10%
)
use company
go 
create table sectionInfo
(
	sectionID int primary key identity(1,1),
	srctionName varchar(10) not null
)
create table userInfo
(
	userNo int primary key identity(1,1),
	userName varchar(10) unique not null check(userName >=4),
	userSex varchar(2) not null check(userSex='��'or userSex='Ů'),
	userAge int  not null check(userAge>1 and userAge<=100),
	userAddress varchar(50) default('����'),
	userSection int constraint co_sectionID foreign key references sectionInfo (sectionID)
)
create table workInfo
(
	workId int primary key identity(1,1),
	userId int constraint id_userNo foreign key references userInfo (userNo) not null,
	workTime datetime not null,
	workDescription  varchar(40) not null check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�')
) 
create table ClassO
(
	classid int primary key identity  (1,1),
	classname nvarchar(20) unique not null,
	datatime datetime not null,
	classDE text
)
use master
go
create database  Student
use Student
go
create table ClassO
(
	classid int primary key identity  (1,1),
	classname nvarchar(20) unique not null,
	datatime datetime not null,
	classDE text
)
create table stInfo
(
	stuid int primary key identity (1,1),
	stuName varchar(20) check(stuName>=4),
	stuSex char(2) check(stuSex='��'or stuSex='Ů') default('��') not null,
	stuAge int check(stuAge>=15 and  stuAge<=40),
	stuAddress text default('�����人'),
	classID int 
)
create table classInfo
(
	classId int primary key identity(1,1),
	className char(40) unique,
	CourseInfo ntext
)
create table performance
(
	performanceId int primary key identity(1,1),
	stuId int not null,
	classId int not null,
	grade int check(grade>=0 and grade <=100 )
)
create database fangwu
on
(
 name='fangwu',
  filename='F:\fangwu.mdf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
log on
(
  name='fangwu_log',
  filename='F:\fangwu_log.ldf',
  size=5mb,
  maxsize=50mb,
  filegrowth=10%
)
create table tblUser 
(
 userId int primary key identity(1,1),
 userName varchar,
 userTel  nvarchar(10)
)
create table tblHouseType 
(
 typeId  int primary key identity(1,1),
 typName varchar check(typName='����'or typName='��ͨסլ'or typName='ƽ��' or typName='������') not null
)
create table tblQx 
(
 qxId int primary key identity(1,1),
 qxName varchar check(qxName='���'or qxName='����' or qxName='����') not null
)

create table tblHouseInfo
(
  id int primary key identity(1,1),
  userId int foreign key references tblUser (userId),
  zj money,
  shi varchar,
  ting int,
  typeId int foreign key references tblHouseType (typeId),
  qxId int foreign key references tblQx (qxId)
)