--3. 为学校开发一个学生管理系统，请为该系统创建数据库，主要存放的信息有：班级信息、学生信息、课程信息、学生考试成绩
create database StuInfo
on
(
	name='StuInfo',
	filename='D:\test\StuInfo.mdf'
)
log on
(
	name='StuInfo_log',
	filename='D:\test\StuInfo_log.ldf'
)
--   班级信息：
create table class1
(
--			   班级编号 classid (主键、标识列)
	classid int primary key(classid) identity(1,1),
--             班级名称(例如：T1、T2、D09等等):不能为空，不能重复
	classname varchar(10) not null unique,
--             开办时间：不能为空
	OpeningTime date not null,
--             班级描述
	classtext text 
)
--	 学生信息：
create table StuInfo1
(
--			   学号：主键、标识列
	stuID int primary key(stuID) identity(1,1),
--             姓名：长度大于2，不能重复
	stuName varchar(20) unique check(len(stuName)>4),
--             性别：只能是‘男’或‘女’，默认为男，不能为空
	stuSex char(2) default('男') check(stuSex in('男','女')),
--             年龄：在15-40之间，不能为空
	stuAge int check(stuAge>=15 and stuAge<=40),
--             家庭地址：默认为“湖北武汉”
	sruAddress varchar(20) default('湖北武汉'),
--             所在的班级编号
	classid int foreign key(classid) references Class1(classid)
)

--   课程信息：编号：主键、标识列
create table course1
(
--             课程名：不能为空，不能重复
	coursename varchar(10) unique not null,
--             课程描述：
	coursetext text
)
   
--   成绩信息：
create table StuExam1
(
--			   成绩编号：主键、标识列
	ExamID int primary key(ExamID),
--             成绩所属于的学生编号，不能为空
	stuID int not null foreign key(stuID) references StuInfo1(stuID),
--             成绩所属的课程编号，不能为空
	classid int not null foreign key(classid) references Class1(classid),
--             成绩：在0-100之间
	Exam int check(Exam>=0 and Exam<=100)
)