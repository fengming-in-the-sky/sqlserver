create database company
on
(
name='company',
filename='D:\test\company.mdf',
size=5MB,
maxsize=100MB,
filegrowth=10%

) 
log on
(
name='company_log.ldf',
filename='D:\test\company_log.ldf',
size=5MB,
maxsize=100MB,
filegrowth=10%
)
create table sectionlnfo
(
sectionID int primary key identity(1,1),
sectionName varchar(10) not null,

)
create table userlnfo
(
userNo int primary key identity (1,1) not null,
userName varchar (10) unique not null ,
userSex varchar(2) not null default('��') check (userSex in ('��','Ů')),
useAge int not null ,
userAddress varchar(50) default('����'),
userSection int references sectionlnfo(sectionID)
)
create table worklonfo
(
workld int primary key not null,
userld int references  userlnfo(userNo),
workTime datetime not null,
workDescription varchar(40) not null default('�ٵ�')

)