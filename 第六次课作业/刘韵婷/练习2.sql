use master
go
create database BBS
on
(
name='BBS',
filename='C:\SQL\BBS.mdf',
size=10MB,
maxsize=100MB,
filegrowth=10MB
)
log on
(
name='BBS_log',
filename='C:\SQL\BBS_log.ldf',
size=10MB,
maxsize=100MB,
filegrowth=10MB
)
go
use BBS
go
create table BBSUsers
(
UID int not null,
UName varchar(10) not null,
USex varchar(2) not null,
UAge int not null,
UPoint int not null
)
alter table BBSUsers add constraint PK_BBSUsers_UID primary key(UID)
alter table BBSUsers add constraint UK_BBSUsers_UName unique(UName)
alter table BBSUsers add constraint CK_BBSUsers_USex check(USex='男' or USex='女')
alter table BBSUsers add constraint CK_BBSUsers_UAge check(UAge>=15 and UAge<=60)
alter table BBSUsers add constraint CK_BBSUsers_UPoint check(UPoint>=0)

go
use BBS
go
create table BBSSection
(
SID int not null,
SName varchar(10) not null,
SUID int not null
)
alter table BBSSection add constraint PK_BBSSection_SID primary key(SID)
alter table BBSSection add constraint FK_BBSSection_SUID foreign key (SUID) references BBSUsers(UID)

go
use BBS
go
create table BBSTopic
(
TID int primary key identity(1,1) not null,
TUID int references BBSUsers(UID) not null,
TSID int references BBSSection(SID) not null,
TTitle varchar(100) not null,
TMsg text not null,
TTime datetime ,
Tcount int ,
)
go
use BBS
go
create table BBSReply
(
RID int primary key identity(1,1) not null,
RUID int references BBSUsers(UID) not null,
RTID int references BBSTopic(TID) not null,
RMsg text ,
RTime datetime ,
)

insert into BBSUsers (UID,UName , USex , UAge , UPoint) values(1,'小雨点','女','20','0')
insert into BBSUsers (UID,UName , USex , UAge , UPoint) values(2,'逍遥','男','18','4')
insert into BBSUsers (UID,UName , USex , UAge , UPoint) values(3,'七年级生','男','19','2')

select UName,UPoint into BBSPoint from BBSUsers

insert into BBSSection (SID,SName,SUID) values(4,'技术交流',1)
insert into BBSSection (SID,SName,SUID) values(5,'读书世界',3)
insert into BBSSection (SID,SName,SUID) values(6,'生活八卦',1)
insert into BBSSection (SID,SName,SUID) values(7,'八卦区',3)

insert into BBSTopic (TUID,TSID,TTitle,TMsg,TTime,Tcount) values(2,7,'范跑跑','谁是范跑跑','2008-7-8',1)
insert into BBSTopic (TUID,TSID,TTitle,TMsg,TTime,Tcount) values(3,4,'.NET','与JAVA的区别是什么','2008-9-1',2)
insert into BBSTopic (TUID,TSID,TTitle,TMsg,TTime,Tcount) values(1,6,'今夏最流行什么','有谁知道今夏最流行什么','2008-9-10',0)

insert into BBSReply (RUID,RMsg,RTime,RTID) values(1,'范跑跑是什么人。。。','2008-10-1',1)
insert into BBSReply (RUID,RMsg,RTime,RTID) values(2,'它们的区别是什么。。。','2008-11-1',2)
insert into BBSReply (RUID,RMsg,RTime,RTID) values(3,'最流行什么。。。','2008-11-20',3)

select * from BBSSection
delete BBSSection where SName='生活百科'

select * from BBSReply
delete BBSReply