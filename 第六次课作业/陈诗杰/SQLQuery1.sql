use master
go

create database TestDB
on primary
(
	name = TestDB,
	filename = 'D:\SQL\TestDB\TestDB.mdf',--没有E盘，就丢这了
	size = 5MB,
	Maxsize = 50MB,
	filegrowth = 1MB
)
log on
(
	name = TestDB_log,
	filename = 'D:\SQL\TestDB\TestDB_log.ldf',--没有E盘，就丢这了
	size = 1MB,
	Maxsize = 10MB,
	filegrowth = 10%
)
go

use TestDB
create table TypeInfo
(
	typeID int primary key identity(1,1),
	typeName varchar(10) not null
)

create table loginInfo
(
	loginID int primary key identity(1,1),
	LoginName nvarchar(10) not null unique(LoginName),
	LoginPwd nvarchar(20) not null default('123456'),
	LoginSex char(1) check(LoginSex = 0 and LoginSex = 1),
	LoginBirthday date,
	LoginType int constraint FK_TypeInfo_typeID references TypeInfo(typeID)
)
go

use master
go
create database company
on 
(	
	name = company,
	filename = 'D:\SQL\company\company.mdf',--没有E盘！
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 1MB
)

log on 
(	
	name = company_log,
	filename = 'D:\SQL\company\company_log.ldf',--没有E盘！
	size = 1MB,
	maxsize = 10MB,
	filegrowth = 10%
)
go

use company
go

create table sectionInfo
(
	sectionID int identity(1,1) primary key,
	sectionName varchar(10) not null
)

create table userInfo
(
	userNo int identity(1,1) primary key not null,
	userName varchar(10) unique(userName) check(len(userName)>4) not null,
	userSex varchar(2) check(userSex = '男' or userSex = '女') not null, 
	userAge int check(userAge between 1 and 100) not null,
	userAddress varchar(50) default('湖北'),
	userSection int constraint FK_sectionInfo_sectionID references sectionInfo(sectionID)
)

create table workInfo
(
	workID int identity(1,1) primary key not null,
	userID int constraint FK_userInfo_userNo references userInfo(userNo),
	workTime datetime not null,
	workDescription varchar(40) check(workDescription = '迟到' or workDescription = '早退' or workDescription = '旷工' or workDescription = '病假' or workDescription = '事假')
)
go

use master
go
create database Student

use Student
go

create table classInfo
(	
	classID int identity(1,1) primary key,
	className char(10) unique(className) not null,
	startTime datetime not null,
	classRemark ntext
)

create table StuInfo
(
	stuID int primary key identity(1,1),
	stuName char(10) check(len(stuName) > 2) unique(stuName),
	stuSex nchar(1) default('男') check(stuSex = '男' or stuSex = '女') not null,
	stuAge int check(stuAge between 15 and 40) not null,
	stuAddress nchar(20) default('湖北武汉'),
	classID int constraint FK_classInfo_classID references classInfo(classID)
)

create table lessonInfo
(
	lessonID int primary key identity(1,1),
	lessonName nchar(20) unique(lessonName) not null,
	lessonRemark ntext
)

create table scoreInfo
(
	scoreID int primary key identity(1,1),
	stuID int constraint FK_stuInfo_stuID references stuInfo(stuID),
	lessonID int constraint FK_lessonInfo_lessonID references lessonInfo(lessonID),
	score int check(score between 0 and 100)
)
go

use master
go
create database house

use house
create table tblUser
(
	tblID int primary key identity(1,1),
	userName nchar(5),
	userTel int
)

create table tblHouseType
(
	typeID int primary key identity(1,1),
	typeName nchar(20)
)

create table tblQx
(
	qxID int primary key identity(1,1),
	qxName nchar(20)
)

create table tblHouseInfo
(
	id int primary key identity(1,1),
	userID int constraint FK_tblUser_tblID references tblUser(tblID),
	zj char(30), --这啥
	shi char(40), --这又啥
	ting char(30), --emm
	typeID int constraint FK_tblHouseType_typeID references tblHouseType(typeID),
	qxid int constraint FK_tblQx_qxID references tblQx(qxID)
)
go

