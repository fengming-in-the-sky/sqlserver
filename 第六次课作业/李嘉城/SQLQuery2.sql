use master
go
create database company
on(
name='company',
filename='D:\ljc\company.mdf',
size = 5,
maxsize=10,
filegrowth = 1
)
log on(
name='company_log',
filename='D:\ljc\company_log.ldf',
size = 5,
maxsize=10,
filegrowth = 1
)
create table sectionInfo
(
sectionID int primary key,
sectionName varchar(10) not null
)
create table userInfo
(
userNo int primary key not null,
userName varchar(10) unique not null,
userSex varchar(2) not null check(userSex='��'or userSex='Ů'),
userAge int not null check(userAge>=0 and userAge<=100),
UserAddress varchar(50) default('����'),
UserSection int foreign key references SectionInfo(SectionId)
)
create table WorkInfo
(
WorkId int primary key identity(1,1) not null,
UserId int foreign key references UserInfo(UserNo) not null,
WorkTime datetime not null,
WorkDescription varchar(40) not null check(WorkDescription='�ٵ�' or WorkDescription='����'or WorkDescription='����' or WorkDescription='����' or WorkDescription='�¼�')
)