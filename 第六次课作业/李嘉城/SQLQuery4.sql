use master
go
create database House
on
(
name='House',
filename='D:\ljc\House.mdf',
size=10Mb,
maxsize=50Mb,
filegrowth=10%
)
log on
(
name='House_log',
filename='D:\ljc\House_log.ldf',
size=10Mb,
maxsize=50Mb,
filegrowth=10%
)
go
use House
go
create table TblUser
(
UserId int primary key identity(1,1),
UserName nvarchar(4) unique not null,
UserTel varchar(11) unique not null
)

create table TblHouseType
(
TypeId int primary key identity(1,1),
TypeName nvarchar(4) unique not null
)

create table TblQx
(
QxId int primary key identity(1,1),
QxName nvarchar(4) unique not null
)
create table HouseInfo
(
Id int primary key identity(1,1),
Desc1 nvarchar(50),
UserId int foreign key references TblUser(UserID),
zj money,
shi int,
ting int,
TypeId int foreign key references TblHouseType(TypeId),
QxId int foreign key references TblQx(QxId)
)