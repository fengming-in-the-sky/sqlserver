create database bbs
on

(
    name='bbs',
    filename='E:\test\bbs.mdf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
log on
(   
    name='bbs_log',
    filename='E:\test\bbs_log.ldf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
	)
go
use bbs
go
create table bbsUser
(
	uID int identity,
	uName varchar(10) not null,
	uSex  varchar(2) not null,
	uAge int not null,
	uPoint int not null
)
 alter table bbsUsers add constraint PK_bbsUsers_uID primary key(uID)
   alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName)
   alter table bbsUsers add constraint DK_bbsUsers_uSex check(uSex='男'or uSex='女')
   alter table bbsUsers add constraint DK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
   alter table bbsUsers add constraint DK_bbsUsers_uAge check(uPoint>=0)

 create table bbsSection
  (sID int identity,
   sName varchar(10) not null,
   sUid   int ,
   )
 alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
 alter table bbsSection add constraint FK_bbsSection_sUid foreign key (sUid) references bbsUsers (UID)

 create table bbsTopic
(  tID int primary key identity,
   tUID int foreign key references bbsUsers (uID),
   tSID int foreign key references bbsSection(sID),
   tTitle  varchar(100) not null,
   tMsg  text not null,
   tTime  datetime ,
   tCount  int
   )
create table bbsReply
(  rID int primary key identity,
   rUID int foreign key references bbsUsers (uID),
   rTID int foreign key references bbsTopic(sID),
   rMsg  text not null,
   rTime  datetime 
   )

 insert into bbsUsers(uName,uSex,uAge,uPoint) values (' 小雨点,女,20,0'),('逍遥,男,18,4'),('七年级生,男,19,2')
 select uName,uPoint into bbsPoint from bbsUsers
 insert into BBSSection (sID,sName,sUID) values(4,'技术交流',1),(5,'读书世界',3),(6,' 生活百科',1),(7,'八卦区',3)
 insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount)values(2,7,'范跑跑 ',' 谁是范跑跑  ','2008-7-8','1'),(2,4,'.NET  ',' 与JAVA的区别是什么呀？  ','2008-9-1 ','2'),(0,6,'今年夏天最流行什么 ',' 有谁知道今年夏天最流行  ','2008-9-10','0')

 select * from bbsUsers
 
 delete from bbsTopic where tUID=2
 delete from bbsReply where rUID=2
 delete from bbsReply where rID=1
 delete from bbsUsers where uName='逍遥'

 alter table bbsSection drop NK
 alter table bbsTopic drop FK__bbsTopic__tUID__1920BF5C
 update bbsUsers set uPoint ='10' where uName='小雨点'
 delete bbsSection where sName='生活百科'
 delete bbsReply