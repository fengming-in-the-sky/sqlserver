use master
go
create database company
on
(
    name='company',
    filename='E:\test\company.mdf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
log on
(   
    name='company_log',
    filename='E:\test\company_log.ldf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
go
use company
go
create table sectionInfo
( 
  sectionID int primary key ,

sectionName varchar(10) unique not null,
)
create table useInfo
(
useNo int primary key identity (1 ,1) not null,
useName varchar(10) unique not null check (Len (useName)>4),
useSex  nvarchar(2)check(useSex='��'or useSex='Ů'),
useAge  int check(useAge >= 1 and useAge <= 100 ),
useAddress varchar(50) default ('����'),
userSection int  foreign key references sectionInfo(sectionID),
)
create table workInfo
(
workId  int primary key identity (1 ,1) not null,
userId  int foreign key references useInfo(useNo)  not null,
workTime datetime not null,
workDescription  varchar(40) check(workDescription='�ٵ�'or workDescription='����'or workDescription='����'or workDescription='����'or workDescription='�¼�'),
)