use master
go
create database TestDB
on
(
    name='TestDB',
    filename='D:\test\TestDB.mdf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
log on
(   
    name='TestDB_log',
    filename='D:\test\TestDB_log.ldf',
    size=20MB,
    maxsize=100MB,
    filegrowth=10%
)
go
use TestDB
go
create table typeInfo
(
typeld int primary key identity (1 ,1),

typeName varchar(10) not null,
)
create table loginInfo
(
LoginId int primary key identity (1 ,1),
LoginName varchar(10) unique not null,
LoginPwd  varchar(20) default ('123456'),
Sex  nvarchar(1) default('��')check(Sex='��'or Sex='Ů'),
birthday date,
)
