create database TestDB
on
(
	name='TestDB',
	filename='D:\TestDB.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
),
(
	name='TestDB_ndf',
	filename='D:\TestDB_ndf.ndf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='TestDB_log',
	filename='D:\TestDB_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use TestDB
go

create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null
)
create table loginInfo
(
	LoginId int primary key identity(1,1),
	LoginName nvarchar(10) unique  not null,
	LoginPwd nvarchar(20) default(123456) not null,
	Loginsex nvarchar(1),
	Loginbady date,
	Loginhy nvarchar(20)
)
create database company
on
(
	name='company',
	filename='D:\company.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
),
(
	name='company_ndf',
	filename='D:\company_ndf.ndf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='company_log',
	filename='D:\company_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use company
go

create table sectionInfo
(
	sectionID int identity primary key,
	sectionName varchar(10) not null
)
create table userinfo
(
	userNo int identity primary key not null,
	userName varchar(10) unique check(userName>4) not null,
	userSex varchar(2) check(userSex='��' or userSex='Ů') not null,
	userAge int check(userAge>=1 and userAge<=100) not null,
	userAddress  varchar(50) default('����'),
	userSection  int foreign key references sectionInfo(sectionID)
)
create table workinfo
(
	workld int identity primary key not null,
	userld int foreign key references userinfo(userNo),
	workTime datetime not null,
	workDescription  varchar(40) check(workDescription in('�ٵ�','����','����','����')) not null
)
create database xsglxt
on
(
	name='xsglxt',
	filename='D:\xsglxt.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
),
(
	name='xsglxt_ndf',
	filename='D:\xsglxt_ndf.ndf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='xsglxt_log',
	filename='D:\xsglxt_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use xsglxt
go

create table bjxx
(
	classid int identity primary key ,
	bjmc nvarchar(20) unique not null,
	kbsj date not null,
	bjms nvarchar(200)
)
create table classxsxx
(
	xh int identity primary key,
	xm varchar(8) check(xm>2) unique ,
	sex nvarchar(1) default('��') check(sex='��' or sex='Ů') not null,
	age int check(age>=15 and age<=40) not null,
	stuaddress nvarchar(200) default('�����人'),
	bjbh int references bjxx(classid)
)
create table classkcxx
(
	id int identity primary key,
	kcm nvarchar(20) unique not null,
	kcms nvarchar(200)
)
create table classcjxx
(
	cjid int identity primary key,
	xsbh int not null,
	ckbh int not null,
	cj int check(cj>=0 and cj<=100)
)


create table tblUser
(
	userId int identity primary key,
	userName nvarchar(8) not null,
	userTel char(11) not null
)
create table tblHouseType 
(
	typeId int identity primary key,
	typName nvarchar(8) not null
)
create table tblQx 
(
	qxId int identity primary key,
	qxName nvarchar(8) not null
)
create table tblHouseInfo
(
	id int identity primary key,
	descc nvarchar(8) not null,
	userld int,	
	zj int,	
	shi int,	
	ting int,	
	typeld int,	
	qxld int
)