create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use bbs
go
create table bbsUsers
(
	UIDD int identity,
	uName varchar(10) not null,
	uSex  varchar(2) not null,
	uAge int not null,
	uPoint int not null
)
create table bbsSection
(
	sIDD  int identity,
	sName  varchar(10) not null,
	sUid   int 
)

alter table bbsUsers add constraint Pk_bbsUsers_UIDD primary key(UIDD)
alter table bbsUsers add constraint Uk_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint Ck_bbsUsers_uSex check(uSex='男'or uSex='女')
alter table bbsUsers add constraint Ck_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint Ck_bbsUsers_uPoint check(uPoint>=0)

alter table bbsSection add constraint Pk_bbsSection_sIDD primary key(sIDD)
alter table bbsSection add constraint Fk_bbsSection_sUid foreign key(sUid) references bbsUsers(UIDD)

create table bbsTopic
(
	tID  int identity primary key,
	tUID  int foreign key references bbsUsers(UIDD), 
	tSID  int foreign key references bbsSection(sIDD),
	tTitle  varchar(100) not null,
	tMsg  text not null,
	tTime  datetime,
	tCount  int
)
create table bbsReply
(
	rID  int identity primary key,
	rUID  int foreign key references bbsUsers(UIDD),
	rTID  int foreign key references bbsTopic(tID),
	rMsg  text not null,
	rTime  datetime
)


insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
insert into bbsTopic values(2,4,'范跑跑 ',' 谁是范跑跑  ','2008-7-8','1'),(3,1,'.NET  ',' 与JAVA的区别是什么呀？  ','2008-9-1 ','2'),
(1,3,'今年夏天最流行什么 ',' 有谁知道今年夏天最流行  ','2008-9-10','0')
insert into bbsReply values(3,2,'不就是你嘛','2008-7-8'),(1,1,'面对对象','2008-7-8'),(1,3,'拖鞋','2008-7-8')
delete from bbsReply where rTID=1
delete from bbsTopic where tUID=2
delete from bbsUsers where uName='逍遥'
update bbsUsers set uPoint=12  where uName='小雨点'
delete from bbsReply where rTID=3
delete from bbsTopic where tSID=3
truncate table bbsReply