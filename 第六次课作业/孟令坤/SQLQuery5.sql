--用SQL实现以下的题目：

--1. 先创建一个数据库，数据库名为TestDB，要求有两个数据文件，一个日志文件，注意命名规范，文件存放在E盘下
--   再在上面的数据库下创建表，结构如下：
use master

go

create database TestDB
on
(
 name='TestDB',
 filename='D:\TEXT\TestDB.mdf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
)
log on
(
 name='TestDB_log',
 filename='D:\TEXT\TestDB_log.ldf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
)
--   会员类别表(typeInfo)：
--   类别编号(typeId)：主键、自动编号
--   类别名(typeName): varchar(10)  不能为空

go
use TestDB

go

create table typeInfo
(
 typeId int primary key identity(1,1),
 typeName varchar(10) not null,
)
--   登录用户表(loginInfo)：
--   编号(LoginId)，数据类型(int)，主键、自动编号
--   账户(LoginName)，文本，长度为10，非空，必须唯一，不能重复
--   密码(LoginPwd)，文本，长度为20，非空、默认值为‘123456’
--   性别(自定类型)
--   生日(自定类型)
--   会员类别(自定类型)
go
use TestDB

go
create table loginInfo
(
 LoginId int primary key identity(1,1),
 LoginName nvarchar(10) unique not null,
 LoginPwd nvarchar(20) default(123456) not null,
 LoginSex nvarchar(1) default('男') check(LoginSex='男' or LoginSex='女'),
 LoginBirthday date,
 LoginMember nvarchar(30) 
)

--2. 先创建一个数据库用来存放某公司的员工信息，数据库的名称为company，包含2个数据文件1个日志
--文件，数据文件和日志文件全部存放在E盘中，初始大小，增长和最大大小自己设定
create database company
on
(
 name='company',
 filename='D:\TEXT\company.mdf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
)
log on
(
 name='company_log',
 filename='D:\TEXT\company_log.ldf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
)
--   再创建表：

--	部门信息表（sectionInfo）
--	部门编号  sectionID  int 标识列  主键
--	部门名称  sectionName  varchar(10)  不能为空
go
use company

go

create table sectionInfo
(
 sectionID int identity primary key,
 sectionName varchar(10) not null
)
--	员工信息表（userInfo）
--	员工编号  userNo  int  标识列 主键  不允许为空
--	员工姓名  userName  varchar(10)  唯一约束 不允许为空 长度必须大于4
--	员工性别  userSex   varchar(2)  不允许为空  只能是男或女
--	员工年龄  userAge   int  不能为空  范围在1-100之间
--	员工地址  userAddress  varchar(50)  默认值为“湖北”
--	员工部门  userSection  int  外键，引用部门信息表的部门编号
go
use company

go

create table userInfo
(
  userNo int identity primary key not null,
  userName varchar(10) unique check(userName>4) not null,
  userSex varchar(2)  default('男') check(userSex='男' or userSex='女') not null,
  userAge int check(userAge>=1 and userAge<=100) not null,
  userAddress varchar(50) default('湖北'),
  userSection int references sectionInfo(sectionID)
)

--	员工考勤表（workInfo）
--	考勤编号  workId int 标识列  主键  不能为空
--	考勤员工  userId  int 外键 引用员工信息表的员工编号 不能为空
--	考勤时间  workTime datetime 不能为空
--	考勤说明  workDescription  varchar(40) 不能为空 内容只能是“迟到”，“早退”，“旷工”，“病假”，“事假”中的一种

go
use company

go

create table workInfo
(
 workId int identity primary key not null,
 userId int references userInfo(userNo) not null,
 workTime datetime not null,
 workDescription varchar(40) check(workDescription in('迟到','早退','矿工','病假','事假')) not null,
)
--3. 为学校开发一个学生管理系统，请为该系统创建数据库，主要存放的信息有：班级信息、学生信息、课程信息、学生考试成绩
create database stundent
on
(
 name='stundent',
 filename='D:\TEXT\stundent.mdf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
)
log on
(
 name='stundent_log',
 filename='D:\TEXT\stundent_log.ldf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
 )
--   班级信息：班级编号 classid (主键、标识列)
--             班级名称(例如：T1、T2、D09等等):不能为空，不能重复
--             开办时间：不能为空
--             班级描述
go
use stundent

go

create table ClassInfo
(
 ClassID int primary key identity,
 ClassName nvarchar(10) unique not null,
 ClassTime date not null,
 ClassDescribe nvarchar(200),
)

--   学生信息：学号：主键、标识列
--             姓名：长度大于2，不能重复
--             性别：只能是‘男’或‘女’，默认为男，不能为空
--             年龄：在15-40之间，不能为空
--             家庭地址：默认为“湖北武汉”
--             所在的班级编号
go
use stundent

go

create table StuInfo
(
 StuID int primary key identity,
 StuName nvarchar(2) unique,
 StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
 StuAge int check(StuAge>=15 and StuAge<=40) not null,
 StuAddress nvarchar(100) default('湖北武汉'),
 ClassID int
)
--   课程信息：编号：主键、标识列
--             课程名：不能为空，不能重复
--             课程描述：
go
use stundent 

go

create table  course
(
 CourseID int primary key identity,
 CourseName nvarchar(10) unique not null,
 CourseDescribe nvarchar(100)
) 
--   成绩信息：成绩编号：主键、标识列
--             成绩所属于的学生编号，不能为空
--             成绩所属的课程编号，不能为空
--             成绩：在0-100之间
go
use stundent 

go

create table  resultInfo
(
 ResultID int primary key identity,
 StuID  int not null,
 ClassID int not null,
 Result int check(Result>=0 and Result<=100)
)
--4. 为一个房屋出租系统创建一个数据库，数据库中主要存放房屋的信息，包括：房屋的编号，房屋的描述，发布人的信息(姓名和联系电话)，房屋的租金，房屋的室数，房屋的厅数，房屋的类型(别墅、普通住宅、平房、地下室)，房屋所属的区县(武昌、汉阳、汉口)，请根据上面的描述设计表，并设计表的关系，以及列的约束
create database home
on
(
 name='home',
 filename='D:\TEXT\home.mdf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
)
log on
(
 name='home_log',
 filename='D:\TEXT\home_log.ldf',
 size=5MB,
 maxsize=50MB,
 filegrowth=10%
 )
--tblUser --发布人信息表
--userId
--userName
--userTel
go
use home  

go

create table tblUser
(
 userId int primary key identity,
 userName nvarchar(4) not null,
 userTel int not null
)
--tblHouseType --房屋的类型
--typeId
--typName
go
use home  

go

create table tblHouseType
(
 typeId int primary key identity,
 typName nvarchar(10) check(typName>2)
)
--tblQx --区县
--qxId
--qxName
go
use home  

go

create table tblQx
(
 qxIdint int primary key identity,
 qxName nvarchar(10) check(qxName>2)
)
--tblHouseInfo--房屋信息表
--id
--desc
--userId  --
--zj
--shi
--ting
--typeId --
--qxId --

go
use home  

go

create table tblHouseInfo
(
 id int primary key identity,
 userId  int references tblUser(userId),
 zj int,
 shi nvarchar(20),
 ting nvarchar(10),
 typeId int references tblHouseType(typeId),
 qxId int,
)






             