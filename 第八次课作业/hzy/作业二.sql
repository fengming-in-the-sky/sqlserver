use master
go
create database odXHSD
on
( name='odXHSD',
  filename='C:\test\odXHSD.mdf',
  size=20,
  maxsize=100,
  filegrowth=10%
  )
  log on
  ( name='odXHSD_log',
  filename='C:\test\odXHSD_log.ldf',
  size=20,
  maxsize=100,
  filegrowth=10%
  )
  go
  use odXHSD
  go
  create table orders
  ( orderId  int primary key identity(1,1),
    orderDate datetime,
	)
   insert into  orders(orderDate)  values('2008-01-12 ')
   insert into  orders(orderDate)  values('2008-02-10 ')
   insert into  orders(orderDate)  values('2008-02-15 ')
   insert into  orders(orderDate)  values('2008-03-10 ')

  
  create table orderItem
  ( ItemiD int primary key identity,
	orderId int ,
	itemType nvarchar(20),
	itemName nvarchar(10),
	theNumber int,
	theMoney money
	 )
   
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (1,'文具','笔','72' ,'2' )
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (1,'文具','尺','10' ,'1' )
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (1,'体育用品','篮球','1','56' )
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (2,'文具','笔','36','2')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (2,'文具','固体胶','20','3')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (2,'日常用品','透明胶','36','2')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (2,'体育用品','羽毛球','20','3')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (3,'文具','订书机','20','3')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (3,'文具','订书针','10','3')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (3,'文具','裁纸刀','5','5')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (4,'文具','笔','20','2')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (4,'文具','信纸','50','1')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (4,'日常用品','毛巾','4','5')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (4,'日常用品','透明胶','30','1')
  insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values (4,'体育用品','羽毛球','20','3' )
	 
	 select * from orders
     select * from orderItem

	 select  sum(theNumber) from orderItem
     select  orderId,sum(theNumber)As 订购总数量,avg(theMoney)As 平均单价 from orderItem group by orderId having orderId<3 and avg(theMoney)<10 order by orderId
	 select  sum(theNumber)As 订购总数量,avg (theMoney)As 平均单价 from orderItem group by theNumber,theMoney having sum(theNumber)>50 and avg(theMoney)<10 order by theNumber,theMoney
	 select  itemType,COUNT(*)数量 from orderItem group by itemType order by itemType desc 
	 select  sum(theNumber)As 订购总数量,avg(theMoney)As 平均单价 from orderItem group by itemType,theMoney having sum(theNumber)>100 
	 select  itemName As 产品名称,count(theNumber) As 订购次数,sum(theNumber)As 总数量,avg(theMoney)As 平均单价 from orderItem group by itemName
