use master
go
create database bbs
on
( name='bbs',
  filename='D:\test\bbs.mdf',
  size=10,
  maxsize=100,
  filegrowth=10%
  )
  log on
  ( name='bbs_log',
  filename='D:\test\bbs_log.ldf',
  size=10,
  maxsize=100,
  filegrowth=10%
  )
  go 
  use bbs
  go
  create table bbsUsers
  (UID int identity(1,1),
   uName varchar(10) not null,
   uSex  varchar(2) not null ,
   uAge  int not null ,
   uPoint  int not null ,
   )
   alter table bbsUsers add constraint PK_bbsUsers_UID primary key(UID)
   alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName)
   alter table bbsUsers add constraint DK_bbsUsers_uSex check(uSex='男'or uSex='女')
   alter table bbsUsers add constraint DK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
   alter table bbsUsers add constraint DK_bbsUsers_uPoint check(uPoint>=0)
  
  create table bbsSection
  (sID int identity,
   sName varchar(10) not null,
   sUid   int ,
   )
 alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
 alter table bbsSection add constraint FK_bbsSection_sUid foreign key (sUid) references bbsUsers (UID)

 create table bbsTopic
(  tID int primary key identity,
   tUID int foreign key references bbsUsers (UID),
   tSID int references  bbsSection(sID) ,
   tTitle  varchar(100) not null,
   tMsg  text not null,
   tTime  datetime ,
   tCount  int
   )
create table bbsReply
(  rID int primary key identity,
   rUID int foreign key references bbsUsers (UID),
   rTID int foreign key references bbsTopic(tID),
   rMsg  text not null,
   rTime  datetime 
   )
   select * from bbsSection
 insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
 select uName,uPoint into bbsPoint from bbsUsers
 set identity_insert bbsSection ON--打开
 insert into bbsSection values (5,' 技术交流',1),(6,'读书世界' ,3),(7,' 生活百科',1),(8,'八卦区' ,3)
 insert into bbsTopic values(2,4,'范跑跑!','谁是范跑跑!', '2008-7-8',1),(3,2,'.NET!','与JAVA的区别是什么呀？','2008-9-1',2),(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？','2008-9-10',0)


select * from bbsUsers
 
 delete from bbsTopic where tUID=2
 delete from bbsReply where rUID=2
 delete from bbsReply where rID=1
 delete from bbsUsers where uName='逍遥'

 alter table bbsSection drop NK
 alter table bbsTopic drop FK__bbsTopic__tUID__1920BF5C

 update bbsUsers set uPoint ='10' where uName='小雨点'
 delete bbsSection where sName='生活百科'
 delete bbsReply

 select tID,count(tCount) from  bbsTopic group by tID
 select count(rID ) from bbsReply group by rID
 select tUID  ,count(tSID) from bbsTopic group by tUID
 select tUID ,sum(tCount) from bbsTopic group by tUID
 select tUID,avg(tCount) from bbsTopic group by tUID having avg(tCount)>3
 select top 1 uName,uSex,uAge,uPoint from bbsUsers 
 select*from bbsTopic where tTitle like '%快乐%' and tMsg  like '%快乐%' 
 select*from bbsUsers where uAge>=15 and uAge<=20 and uPoint>10
 select*from bbsUsers where uName like'小_大' 
 select tUID ,tCount from bbsTopic where tTitle like '%!'
