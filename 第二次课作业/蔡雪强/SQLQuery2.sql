use master
go
if exists(select * from sys.databases where name='Student')
	drop database Student
create database Student
on
(
	name='Student',
	filename='D:\SQL\Student.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
log on
(
	name='Student_log',
	filename='D:\SQL\Student_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
go

use Student

create table StuInfo
(
	StuID int primary key identity(1,1),
	StuNum varchar(10) not null,
	StuName nvarchar(20) not null,
	StuSex char(2) default('��') check(StuSex='��' or StuSex='Ů') not null,
	StuPhone char(11) check(StuPhone=11 and StuPhone=7) not null,
)
go

use Student

create table ClassInfo
(
	ClassID int primary key identity(1,1),
	ClassNum varchar(15) not null,
	ClassName nchar(30) not null,
	ClassRemark text not null,
)