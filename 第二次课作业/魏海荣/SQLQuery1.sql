if exists(select * from sys.databases where name='Students')
	drop database Students

create database Students
go
use Students
go
create table Stulnfo
(
	StuID int primary key identity(1,1),
	StuNum varchar(10) not null,
	StuSex nchar(1) default('��') check(StuSex='��' or StuSex='Ů'),
	ClassName nvarchar(30) not null,
	ClassRemark ntext 
)
create table Classlnfo
(
	ClassID	int primary key identity(1,1),
	ClassNum varchar(15)not null,
	ClassName nvarchar(30) not null,
	ClassRemark ntext ,
	StuID int,
	foreign key (StuID) references Stulnfo(StuID)
)