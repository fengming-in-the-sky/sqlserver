
--事务，表示数据库操作的基本单元，一波操作，原子性
if exists(select * from sys.databases where name='Students')
   drop database Students
 --判断这个数据库是否存在，如果存在则删除原有数据库

Create database Students
--新建数据库
on( 
    name='Students',
	filename='F:\Students.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
log on
(
    name='Students_log',
	filename='F:\Students_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
--定义数据库的属性

use Students  --直接建在这个数据库里
go

create table StuInfo
(
    --列名，数据类型,约束
	StuID int primary key identity(1,1) not null,
	StuNum varchar(10)not null ,
	StuName nvarchar(20) not null,
	StuSex char(2) default('男') check(StuSex='男' or StuSex='女') not null,
	StuPhone bigint
)
create table ClassInfo
(
    ClassID int primary key identity(1,1)not null,
	ClassNum varchar(15)not null,
	ClassName nvarchar(30)not null,
	CkassRenark text ,
	StuID  int,
)