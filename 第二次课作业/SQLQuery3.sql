﻿use master
go if exists(select * from sys.databases where name='Students')
	drop database Students
create database Students
on
(
	name='Students',
	filename='C:\Users\33054\Desktop',
	size=6MB,
	maxsize=100MB,
	filegrowth=10MB
)
log on 
(
	name='Students',
	filename='C:\Users\33054\Desktop',
	size=6MB,
	maxsize=100MB,
	filegrowth=10MB
)
go

use Students
go
create table StuInfo
(
	StuID int primary key identity(1,1) not null,
	StuNum varchar(10) not null,
	StuName nvarchar(20) not null,
	StuSex char(2) default('') check(StuSex=''or StuSex='Ů') not null,
	StuPhone varchar(11) not null
)
use Students
  go
  create  table   ClassInfo
  (
       ClassID      int primary key identity(1,1) not null,
	   ClassNum     char(15) not null,
	   ClassName    char(30)  not null,
	   ClassRemark  text
  )