use master
go
create database Students
on
(
   name='Students',
   filename='D:\TEXT\Students.mdf',
   size=5MB,
   maxsize=100MB,
   filegrowth=10Mb
)
log on
(
   name='Students_log',
   filename='D:\TEXT\Students_log.ldf',
   size=5MB,
   maxsize=100MB,
   filegrowth=10Mb
)

go
use Students
go

create table StuInfo
(
   StuID int primary key identity(1,1) not null,
   StuNum char(10) not null,
   StuName nchar(20) not null,
   StuSex char(2) default('��') check(StuSex='��' or StuSex='Ů') not null,
   StuPhone char(11) check(StuPhone=11)

)
go
use Students
go

create table ClassInfo
(
  ClassID int primary key identity(1,1) not null,
  ClassNum char(15) not null,
  ClassName nchar(30) not null,
  ClassRemark TEXT,
  StuID int
)