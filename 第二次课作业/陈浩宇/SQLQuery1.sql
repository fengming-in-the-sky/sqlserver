use master
go
create database Students
on(
   name='Students',
   filename='D:\SQL\Students.mdf',
   size=5MB,
   maxsize=50MB,
   filegrowth=10MB
)
log on(
 name='Students_log',
   filename='D:\SQL\Students_log.ldf',
   size=5MB,
   maxsize=50MB,
    filegrowth=10MB
)
go
use Students
go
create table StuInfo
(
   StuID int primary key identity(1,1) not null,
   StuNum char(10) not null,
   StuName nchar(20) not null,
   StuSex char(2),
   StuPhone char(11) not null

)
go
use Students

create table ClassInfo
(
	ClassID int primary key identity(1,1),
	ClassNum varchar(15) not null,
	ClassName nchar(30) not null,
	ClassRemark text not null,
)
go