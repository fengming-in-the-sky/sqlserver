create table Classinfo
(
	--列名 数据类型 约束,
	ClassID int primary key identity(1,1) not null,
	ClassName nvarchar(20) not null,
	ClassNum varchar(15),
	ClassRemark nvarchar(50),
	StuID int,
)