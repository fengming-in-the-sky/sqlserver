if exists(select * from sys.databases where name ='Students')
   drop database Students 
create database Students
on(
      name='Students',
	  filename='D:\Students.mdf',
	  size=5MB,
	  maxsize=50MB,
	  filegrowth=10MB
)
log on
(
name='Students_log',
	  filename='D:\Students_log.mdf',
	  size=5MB,
	  maxsize=50MB,
	  filegrowth=10MB
)
go
use Students
go
create table StuInfo
(
StuID int primary key identity(1,1) not null,
StuNum nvarchar(20) not null,
StuName nvarchar(20) not null,
Stusex char(2) default('��') check(Stusex='��' or Stusex='Ů') not null,
StuPhone char(11)
)
create table ClassInfo
(
ClassID int primary key identity(1,1) not null,
ClassNum  nvarchar(15) not null,
ClassName  nvarchar(30) not null,
ClassRemark text,
StuID int not null,
)