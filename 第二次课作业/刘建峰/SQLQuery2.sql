
use master go 
Create database demo678
on(
name='demo678',
filename='D:\',
size='2mb',
filegrowth='1mb',
maxsize='5mb'
)
log on(
name='demo678',
filename='D:\',
size='2mb',
filegrowth='1mb',
maxsize='5mb'
)
go
use demo678
go
Create table Student
(
StudentID int check(StudentID >0) not null,
StudentAge int check(StudentAge>18 and StudentAge<23) not null,
StudentSex char(2) default('��') not null,
)