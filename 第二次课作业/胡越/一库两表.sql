create database Students
go
use Students
go

create table StuIfo
(
	StuId int primary key identity(1,1),
	StuNum char(10) not null,
			StuName varchar(20) not null,
			StuSex char(2) default	'��' check (StuSex in ('��','Ů')),
			StuPhone varchar(20)
)

go

create table ClassInfo
(
	ClassId int primary key identity(1,1),
	ClassNum char(15) not null,
			ClassName varchar(30) not null,
			ClassRemark varchar(max),
			StuId int not null
)

go