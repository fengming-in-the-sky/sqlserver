use master
go

create database Students
on(
	name='Students',
	filename='C:\TEXT\Students.mdf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10MB
)
log on(
	name='Students_log',
	filename='C:\TEXT\Students_log.ldf',
	size=5MB,
	maxsize=100MB,
	filegrowth=10MB
)
go
use Students
go
create table StuInfo
(
	StuID int primary key identity(1,1) not null,
	StuNum varchar(10) not null,
	StuName nvarchar(20) not null,
	StuSex char(2) default('��') check(StuSex='��' or StuSex='Ů') not null,
	StuPhone varchar(20) not null
)
go
use Students
go
create table ClassInfo
(
	ClassID int primary key identity(1,1) not null,
	ClassNum varchar(15) not null,
	ClassName nvarchar(30) not null,
	ClassRemark ntext not null,
	StuID int not null
)