
create database Students
on
(
	name='Students',
	filename='F:\Students.mdf',
	size=6mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='Students_log',
	filename='F:\Students_log.ldf',
	size=6mb,
	maxsize=100mb,
	filegrowth=10mb
)
go

use Students
go
create table StuInfo
(
	StuID int primary key identity(1,1) not null,
	StuNum varchar(10) not null,
	StuName nvarchar(20) not null,
	StuSex char(2) default('��') check(StuSex='��' or StuSex='Ů') not null,
	StuPhone char(11),
)
create table ClassInfo
(
	ClassID int primary key identity(1,1) not null,
	ClassNum nvarchar(15) not null,
	ClassName nvarchar(30) not null,
	ClassRemark text,
)