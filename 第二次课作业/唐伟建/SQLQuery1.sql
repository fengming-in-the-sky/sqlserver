create database Students
on PRIMARY
(filename='D:\homework\Students.mdf',
 name='Lesson2homework',
 size=6MB,
 maxsize=8MB,
 filegrowth=10%
)

log on
(
name='Students_log',
filename='D:\homework\Students_log.ldf',
size=6MB,
maxsize=8MB,
filegrowth=1MB
)
go

--create a form
use Students
go
create table StuInfo
(StuNo int primary key identity(1,1) not null,
StuName nvarchar(20) not null,
StuSex char(2) default('') not null,
StuPocketMoney money check(StuPocketMoney>=0 and StuPocketMoney<=1500),
)

create table ClassInfo
(ClaNo int primary key identity(1,1) not null,
ClaNumPe int not null
)
