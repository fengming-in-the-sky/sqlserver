 create database Student
 go
 use Student
 go
 create table Class
 ( ClassId int primary key identity(1,1),
   ClassName nvarchar(20) unique not null, 
 )
 --���Ӽ�¼
 insert into Class(ClassName)
 select 'a' union
 select 'b' union
 select 'c' union
 select 'd' union
 select 'e' union
 select 'f' union
 select 'g' union
 select 'h' union
 select 'i' union
 select 'j' 
 --�޸ı��
 update Class set ClassName='a' where ClassId='1'
 --ɾ�����
 delete Class where ClassId='10'
 create table Student
 (
  StuId int primary key identity(1,1),
  ClassId int foreign key references Class(ClassId),
  StuName nvarchar(20) not null,
  StuSex nvarchar(1) default('��') check(StuSex='��' or StuSex='Ů'),
  StuBirthday date ,
  StuPhone nvarchar(11) not null unique,
  StuAddress nvarchar(200),
  CreateDate datetime,
 )
 --����ʱ���ֶ�
 alter table Student add CreateDate datetime default(getdate())
 update Student set CreateDate='2021-03-10 16:35:10.568'
 --ɾ�����
 delete Student where ClassId='2'
 set identity_insert Student off
