use master
go
create database Student03
on(
name='Student03',
	filename='C:\test\Student03.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10%
	)
log on(
	name='Student03_log',
	filename='C:\test\Student03_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10%
	)
go
use Student03
go
create table ClassInfo
  (ClassID int primary key identity(1,1), 
  ClassName nvarchar(20) unique not null,
  )
  insert into ClassInfo (ClassName) values ('1��'),('2��'),('3��'),('4��'),('5��'),('6��'),('7��'),('8��'),('9��'),('10��')
  update ClassInfo set ClassName = ('S��') where ClassId = 1
  delete ClassInfo where ClassId = 10
create table StudentInfo
(
	StuID int  primary key identity(1,1) not null ,
	ClassID int references ClassInfo(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('��') check(StuSex='��' or StuSex='Ů'),
	StuBirthday date,
	StuAddress nvarchar(200),
	CreateDate datetime default getdate()
	)
  insert into StudentInfo(ClassID,StuName,StuSex,StuBirthday,StuAddress) values(1,'ɢ��','��','2000-8-2','�����'),(2,'ɢ��','��','2002-3-12','�����'),(3,'ɢ��','Ů','2002-2-12','�����'),(4,'ɢ��','��','2004-8-9','�����'),(5,'ɢ��','��','2007-7-27','�����'),(6,'ɢ��','��','2005-2-21','����ʡ'),(7,'ɢ��','Ů','2002-2-2','�����'),(8,'ɢ��','Ů','2001-6-18','�����'),(1,'ɢ��','��','2001-6-18','�����'),(1,'ɢ��','��','2002-2-2','�����'),(2,'ɢʰҼ','Ů','2002-9-1','�����'),(4,'ɢʰ��','Ů','2001-6-18','�����'),(1,'ɢʰ��','��','2002-3-12','�����'),(5,'ɢʰ��','Ů','2002-3-12','�����'),(8,'ɢʫ��','Ů','2002-2-2','�����'),(6,'ɢʰ½','��','2000-8-2','�����'),(9,'ɢʯ�','��','2002-2-2','�����'),(1,'ɢʮ�Q','Ů','2000-8-2','�����'),(9,'ɢʱ��','Ů','2000-8-2','�����'),(4,'ɢ����','��','2000-8-2','�����')
update StudentInfo set  CreateDate=getdate()
delete StudentInfo where ClassID = 1
  create table CourseInfo
   (CourseID int primary key identity (1,1),
   CourseName nvarchar(50) unique,  
   CourseCredit int default('1') check(CourseCredit='1' or CourseCredit='1,2,3,4,5,')not null,
   CourseType nvarchar(10) check(CourseType='רҵ��' or CourseType='������')
   )
   insert into CourseInfo(CourseName) values('ë��'),('��ѧ'),('Ӣ��'),('����'),('ְҵ�滮'),('����')
   select * from CourseInfo
   update CourseInfo set CourseCredit=3 where CourseName='ë��'
   create table ScoreInfo
(ScoreID int  identity (1,1),
 StuID int foreign key references StudentInfo(StuID),
	CourseID int foreign key references CourseInfo(CourseID),
 Score decimal(5,2) unique not null,
)
insert into ScoreInfo(StuID,CourseID,Score) values(1,1,8),(2,2,7),(3,3,6),(4,4,5),(5,5,4),(6,6,3),(7,7,2),(8,8,1),(9,9,9),(10,1,1),(11,2,2),(12,3,3),(13,4,6),(14,5,7),(15,6,8),(16,7,9),(17,8,8),(18,1,2),(19,2,3),(20,3,4)
delete ScoreInfo where StuId = 1
delete ScoreInfo where CourseId = 1
alter table Score add constraint UK_ScoreInfo_Score default(0) for Score,check(1<Score and Score<100)