create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=8mb,
	maxsize=100mb,
	filegrowth=10mb
)
go
use Student
go
create table Class
(
	ClassId int primary key identity(1,1) not null,
	ClassName nvarchar(20) unique not null
)
create table Student
(
	StuId int primary key identity(1,1) not null,
	ClassId int foreign key references Class(ClassId),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check (StuSex='男' or StuSex='女'),
	StuBirthday date null,
	StuPhone nvarchar(11) unique null,
	StuAddress nvarchar(200) null,
)
create table Course
(
	CourseId int primary key identity(1,1) not null,
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check (CourseCredit>1 and CourseCredit<6) not null,
	CourseType nvarchar(10) check(CourseType='专业课' or CourseType='公共课')
)
create table Score
(
	ScoreId int primary key identity(1,1) not null,
	StuId int foreign key references Student(StuId),
	CourseId int foreign key references Course(CourseId),
	Score decimal(5,2) unique not null
)
insert into Class values ('软件1班'),('软件2班'),('软件3班'),('软件4班'),('软件5班'),('软件6班'),('软件7班'),('软件8班'),('软件9班'),('软件10班')
update Class set ClassName='应用1班' where ClassId=1
delete from Class where ClassId=10

insert into Student(ClassId,StuName,StuSex,StuBirthday,StuPhone,StuAddress) values(1,'张一','男',2002-1-18,101,'龙头'),(2,'张二','男',2002-2-18,102,'龙头'),(3,'张三','男',2002-3-18,103,'龙头'),(4,'张四','男',2002-4-18,104,'龙头'),(5,'张五','男',2002-5-18,105,'龙头'),
(6,'张六','男',2002-6-18,106,'龙头'),(7,'张七','男',2002-7-18,107,'龙头'),(8,'张八','男',2002-8-18,108,'龙头'),(9,'张九','男',2002-9-18,109,'龙头'),(10,'张十','男',2002-10-18,110,'龙头'),(11,'张十一','男',2002-11-18,111,'龙头'),(12,'张十二','男',2002-12-18,112,'龙头'),(13,'张十三','男',2002-1-19,113,'龙头'),
(14,'张十四','男',2002-2-19,114,'龙头'),(15,'张十五','男',2002-3-19,115,'龙头'),(16,'张十六','男',2002-4-19,116,'龙头'),(17,'张十七','男',2002-5-19,117,'龙头'),(18,'张十八','男',2002-6-19,118,'龙头'),(19,'张十九','男',2002-7-19,119,'龙头'),(20,'张二十','男',2002-8-19,120,'龙头')
alter table Student add CreateDate datetime default(getdate())
update Student set CreateDate=getdate()
delete from Student where ClassId=5

insert into Course(CourseName) values('语文'),('数学'),('英语'),('音乐'),('体育'),('专业')
select * from Course
update Course set CourseId=4 where CourseName='思修'

insert into Score(StuId,CourseId,Score) values (1,50,1.1),(2,50,1.1),(3,50,1.1),(4,50,1.1),(5,50,1.1),(6,50,1.1),(7,50,1.1),(8,50,1.1),
(9,50,1.1),(10,50,1.1),(11,50,1.1),(12,50,1.1),(13,50,1.1),(14,50,1.1),(15,50,1.1),(16,50,1.1),(17,50,1.1),(18,50,1.1),(19,50,1.1),(20,50,1.1)
update Score set Score=70.1 where CourseId=5
delete from Score where StuId=1
delete from Score where CourseId=1
alter table Score add constraint FK_Score_Score default(0)for Score, check (Score>=0 and Score<=100)