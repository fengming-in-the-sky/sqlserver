use master
go

create database zuoye02
on
(
name='zuoye02',
filename='D:\test\zuoye02.mdf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
log on
(
name='zuoye02'_log,
filename='D:\test\zuoye02_log.ldf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
go 
use zuoye02
create table Class
(
ClassID int primary key identity (1,1),
ClassName nvarchar(20) unique not null
)
insert into Class (ClassName) values ('一班'),('二班'),('三班'),('四班'),('五班'),('六班'),('七班'),('八班'),('九班'),('十班')
update Class set ClassName='十一班' where ClassID=1
delete from Class where ClassID=10
go 
use zuoye02
create table Student
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex in('男','女')),
	StuBirthday date ,
	StuPhone nvarchar(11) unique ,
	StuAddress nvarchar(200) ,

)
insert into Student  (ClassID,StuName, StuSex, StuBirthday,StuPhone,StuAddress) 
values (1, '张' ,'男','2002-05-24','153453557','江西南昌') ,
(2,'李','男','2002-11-25','153053554','江西'),
 (3, '三' ,'男','2002-04-24','153442557','江西') ,
(4,'六','男','2002-05-27','1535355469','江西') ,
 (5,'七','男','2002-05-28','15345355417','江西') ,
(6,'八','女','2002-05-29','15345355427','江西') ,
 (7,'九','男','2002-06-01','15345355437','江西')

delete Student where ClassID=1
alter table Student add CreateDate datetime default(getdate())

select * from Student
go 
use zuoye02

create  table Course
(
	Courseld int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default(1) check(CourseCredit>=1 and CourseCredit<=5),
	CourseType nvarchar(10) check(CourseType in
	('专业课' , '公共课'))
)
insert into Course (CourseName)
select'张'union
select'是'union
select'的' union
select'给' union
select'人' union
select'发' 

select * from Course
go 
use zuoye02
create table Score
(
ScoreId int primary key identity(1,1),
StuId int references Student(StuID),
Courseld int references Course (Courseld),
Score decimal(5,2)unique not null
)
select * from Score
update Course set CourseCredit=5  where  CourseName='英语'
delete Score where StuId=1
delete Score where Courseld=1
alter table Score add constraint  DF default(0) for Score, check(Score>=0 or Score<=100) 