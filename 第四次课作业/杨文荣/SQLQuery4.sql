use master
go

create database Student
on
(
	name='Student',
	filename='C:\学习\数据库\Student.mdf',
	size=5MB,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='C:\学习\数据库\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use Student
go
create table Class
(
	ClassId int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)

create table Student
(
	StuId int primary key identity(1,1),
	ClassId int foreign key references Class(ClassId),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男'or StuSex='女'),
	StuBirthday date,
	Stuphone nvarchar(11) unique ,
	StuAddress nvarchar(200)
)
create table Course
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseScore int not null default(1) check(CourseScore>=1 and CourseScore<=5 ),
	CourseCredit nvarchar(10) check(CourseCredit='专业课' or CourseCredit='公共课')
)
create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int foreign key references Student(StuId),
	CourseId int foreign key references Course(CourseId),
	Score decimal(5,2) unique not null
)
insert into	Class(ClassName) values('软件1班'),('软件2班'),('软件3班'),('软件4班'),('软件5班'),('软件6班'),('软件7班'),('软件8班'),('软件9班'),('软件10班')

update Class set ClassName =('软件1班') where  ClassId =1

delete from Class where ClassId =10

select * from Class
 
insert into Student(StuName,StuSex,StuPhone,ClassID) 
values 
('张三','男','10248191092',1),('李四','男','10248191063',2),('王五','男','10248190691',1),('赵六','女','10248191091',3),('钱七','男','10248190784',4),
('小明','男','10248190801',9),('小红','女','10248190840',8),('小王','男','10248190919',7),('小赵','男','10248190876',6),('小李','男','10248191003',5),
('老王','女','10248190767',2),('老明','男','10248190756',3),('老阿姨','男','10248191072',4),('这啥名','男','10248190613',5),('我不知道','男','10248191129',6),
('编不下去了','男','10248190866',4),('真的','女','10248190626',5),('莓办法','男','10248191107',2),('稿不了','女','10248191054',3),('再见','男','10248190856',7)


alter table Student add CreateDate datetime default(getdate())

update Student set CreateDate=(getdate())

delete Student where ClassId = 2

select*from Student

insert into Course(CourseName)
values
('数学'),('英语'),('语文'),
('体育'),('政治'),('职素')

select CourseName from Course

update Course set CourseScore=5 where CourseName=('数学')

select*from Student

insert into Score(Score)
values
(1.0),(2.0),(3.0),(4.0),(5.0),
(12.0),(23.0),(84.0),(24.0),(25.0),
(31.0),(32.0),(33.0),(34.0),(35.0),
(41.0),(42.0),(43.0),(44.0),(45.0)
select * from Score

delete Score where StuId=1

delete Score where CourseId=1

alter table Score add constraint DF default(0) for Score, check(Score>=0 or Score<=100)





