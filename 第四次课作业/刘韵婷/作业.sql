create database Student01
on
(
   name='Student01',
   filename='F:\SQL\Student01.mdf',
   size=10mb,
   maxsize=50mb,
   filegrowth=10%
)
log on
(
   name='Student01_log',
   filename='F:\SQL\Student01_log.ldf',
   size=10mb,
   maxsize=50mb,
   filegrowth=10%
)
use Student01
go

create table Class
(
   ClassID int primary key identity(1,1),
   ClassName nvarchar(20) unique not null,
)

insert into Class(ClassName) 
select '1班' union 
select '2班' union
select '3班' union
select '4班' union
select '5班' union
select '6班' union
select '7班' union
select '8班' union
select '9班' union
select '10班'

update Class set ClassName='1班' where ClassID='1'

delete Class where ClassID='10'

select * from Class

create table Student
(
    StuId int primary key identity(1,1),
	ClassId int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex  nvarchar(1)  check(StuSex='男' or StuSex='女')default('男'),
	StuBirthday date ,
	StuPhone nvarchar(11) unique , 
	StuAddress nvarchar(200)
)

set IDENTITY_INSERT Student ON

insert into Student(StuId,ClassId,StuName,StuSex,StuBirthday,StuPhone,StuAddress)
select '10086', 1,'小一','男', '2020/5/6',15168,'福建省' union
select '10087', 2,'小二','男',	'2020/10/6',15169,'福建省' union
select '10088', 3,'小三','男',	'2021/3/6',15170,'福建省' union
select '10089', 4,'小四','男',	'2021/8/6',15171,'福建省' union
select '10090', 5,'小五','男',	'2022/1/6',15172,'福建省' union
select '10091', 6,'小六','女',	'2022/6/6',15173,'福建省' union
select '10092', 7,'小七','女',	'2022/11/6',15174,'福建省' union
select '10093', 8,'小八','女',	'2023/4/6',15175,'福建省' union
select '10094', 9,'小九','女',	'2023/9/6',15176,'福建省'

select * from Student
alter table Student add CreateDate datetime default(getdate())

delete Student where ClassId='2'

set IDENTITY_INSERT Student OFF

create table Course
(
  CourseId int primary key identity(1,1),
  CourseName nvarchar(50) unique not null,
  CourseCredit int not null default(1) check(CourseCredit>=1 or CourseCredit<=5),
  Classtypes  nvarchar(10) check(Classtypes in('职素课' , '政治课'))
)

insert into Course(CourseName)
select 	'语文' union
select 	'数学' union
select 	'英语' union
select	'专业' union
select 	'思修' union
select	'体育'

select * from Course

update Course set CourseCredit=2  where CourseName='体育'

create table Score
(
   ScoreId int primary key identity(1,1),
   StuId int foreign key references Student(StuId),
   CourseId int foreign key references Class(ClassId),
   Score decimal(5,2) unique not null
)

insert into Score(Score) 
select	60	union
select	61	union
select	62	union
select	63	union
select	64	union
select	65	union
select	66	union
select	67	union
select	68	union
select	69	union
select	70	union
select	71	union
select	72	union
select	73	union
select	74	union
select	75	union
select	76	union
select	77	union
select	78	union
select	79	
		
select * from Score

update Score  set Score=100 where  ScoreId=20
delete Score where StuId=1
delete Score where CourseId=1
alter table Score add constraint  DF default(0) for Score, check(Score>=0 or Score<=100) 