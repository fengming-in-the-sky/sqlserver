use master
go
if exists(select * from sys.databases where name='Student')
	drop database Student
	create database Student
on
(
	name='Student',
	filename='D:\SQL\Student.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
log on
(
	name='Student_log',
	filename='D:\SQL\Student_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10MB
)
go
use Student
create table Class
(
ClassId int primary key identity(1,1),
ClassName nvarchar(20) unique(ClassName) not null,
)
insert into Class(ClassName) values('һ��'),('����'),('����'),('�İ�'),('���'),('����'),('�߰�'),('�˰�'),('�Ű�'),('ʮ��')
update Class set ClassName = ('1��') where ClassID = 1
delete Class where ClassID = 10
create table Student
(
StuId int primary key identity(1,1),
ClassId int constraint FK_Class_ClassId references Class(ClassId),
StuName nvarchar(20) not null,
StuSex nvarchar(1) default('��') check(StuSex='��' or StuSex='Ů'),
StuBirthday date,
StuPhone nvarchar(11) unique,
StuAddress nvarchar(200)
)
insert into Student(StuName,StuSex,StuBirthday,StuPhone,StuAddress) values('��1','��','2020/1/1','100001','����'),('��2','��','2020/1/1','100002','����'),('��3','��','2020/1/1','100200','����'),('��4','��','2020/1/1','10200','����'),('��5','��','2020/1/1','10123000','����'),('��6','��','2020/1/1','102200','����'),('��7','��','2020/1/1','130000','����'),('��8','��','2020/1/1','1004300','����'),('��9','��','2020/1/1','1002100','����'),('��10','��','2020/1/1','10546000','����')
alter table Student add CreateDate datetime default(getdate())
update Student set CreateDate='2021/3/11'
delete Student where ClassId='10'
create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique(CourseName) not null,
	CourseCredit int default(1) check(CourseCredit >= 1 and CourseCredit <=5),
	CourseType nvarchar(10) Check(CourseType = 'רҵ��' or CourseType = '�Ļ���')
)
insert into Course(CourseName) values ('��ѧ'),
('Ӣ��'),('����'),('����'),('����'),('����')
select * from Course
update Course set CourseCredit = 2 where CourseName = '����'
create table Score
(
	ScoreID int primary key identity(1,1),
	StuID int constraint FK_Student_StuID references Student(StuID),
	CourseID int constraint FK_Course_CourseID references Course(CourseID),
	Score decimal(5,2) unique(Score) not null
)
insert into Score(StuID,CourseID,Score) values (1,1,1),(1,1,2),(1,1,3),(1,1,4),(1,1,5),(1,1,6)
delete Score where StuID = 1
delete Score where CourseID = 1
alter table Score add constraint CK_Score_Score check(Score >= 0 and Score <= 100)
alter table Score add constraint DK_Score_Score default'0' for Score