use master
go
create database Students
use Students
go
create table Class
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)

insert into Class (ClassName) values ('一班'),('二班'),('三班'),('四班'),('五班'),('六班'),('七班'),('八班'),('九班'),('十班')
update Class set ClassName='一班多' where ClassID=1
delete from Class where ClassID=10

create table Student
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
	StuBirthday date,
	StuPhone nvarchar(11) unique not null,
	StuAddress nvarchar(200),
)
insert into Student (ClassID,StuName,StuSex,Stubirthday,Stuphone) values
('1','我','男','20000101','12121212121'),
('1','我1','男','20000101','12121212122'),
('1','还2','男','20000101','12121212123'),
('1','我3','男','20000101','12121212125'),
('1','我4','男','20000101','12121212126'),
('1','我5','男','20000101','12121212127'),
('1','我6','男','20000101','12121212128'),
('1','我7','男','20000101','12121212129'),
('1','我8','男','20000101','12121212130'),
('1','我9','男','20000101','12121212131'),
('1','我10','男','20000101','12121212132'),
('1','我11','男','20000101','12121212133'),
('1','我12','男','20000101','12121212134'),
('1','我13','男','20000101','12121212135'),
('1','我14','男','20000101','12121842136'),
('1','我15','男','20000101','12121212137'),
('1','我16','男','20000101','12151212138'),
('1','我17','男','20000101','12121212139'),
('1','我18','男','20000101','12121212140'),
('1','我19','男','20000101','12121882141')

alter table Student  add CreaDate datetime default(getdate())

delete from Student where ClassID=1

create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(1<=CourseCredit and CourseCredit<=5),
	CourseType nvarchar(10) check(CourseType='专业课' or CourseType='公共课')
)
insert into Course (CourseName) values
('音乐'),('体育'),('思修'),('数学'),('美术'),('英语')
select CourseName from Course
update Course set CourseCredit=4 where CourseName='数学'

create table Score
(
	ScoreID int primary key identity(1,1),
	StuID int foreign key references Student(StuID),
	CourseID int foreign key references Course(CourseID),
	Score decimal(5,2) unique not null 
)
insert into Score (Score) values
('1'),
('2'),
('3'),
('4'),
('5'),
('6'),
('7'),
('8'),
('9'),
('10'),
('11'),
('12'),
('13'),
('14'),
('15'),
('16'),
('17'),
('18'),
('19'),
('20')
update Score set Score=4 where CourseID=3
delete from Score where StuID=1
delete from Score where CourseID=1
alter table Score add constraint UK_Score_Score default(0) for Score,check(1<Score and Score<100)