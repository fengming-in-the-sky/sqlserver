use master
go
create database Student1
on(
name='Student2',
filename='D:\作业\SQL作业',
size=5,
maxsize=10,
filegrowth=1
)
log on(
name='Student2_log',
filename='D:\作业\SQL作业',
size=5,
maxsize=10,
filegrowth=1
)
go
use Student1
go
create table Class
(
ClassId int primary key identity(1,1),
ClassName  nvarchar(20) unique not null
)
--insert into Class (ClassName) Values ('软件1班'),('软件2班'),('软件3班'),('软件4班'),('软件5班'),('软件6班'),('软件7班'),('软件8班'),('软件9班'),('软件10班')
insert into Class (ClassName)
select ('软件1班')union
select ('软件2班')union
select ('软件3班')union
select ('软件4班')union
select ('软件5班')union
select ('软件6班')union
select ('软件7班')union
select ('软件8班')union
select ('软件9班')union
select ('软件10班')
select*from Class
truncate table  Class
UPdate Class  set ClassName='软件101班' where ClassId=1
delete from Class
create table Student
(
StuId int primary key identity(1,1),
ClassId int foreign key references Class(ClassId),
StuName nvarchar(20) not null,
Stusex nvarchar(1) default('男') check(Stusex='男' or Stusex='女'),
Stubirthday date,
Stuphone nvarchar(11) unique,
StuAddress nvarchar(200),
Createdate datetime default getdate()
)
insert into Student (ClassId,StuName,Stusex,Createdate) values ('','zs','男','2021/3/10')
update Student set Createdate 
alter table Student drop constraint FK__Student__ClassId__2B3F6F97
select*from Student
delete from Student where ClassId=0
create table Course
(
CourseId int primary key identity(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int default('1') check(CourseCredit>=1 or CourseCredit<=5),
CourseCredit2 nvarchar(10) CHECK(CourseCredit2='专业课' OR CourseCredit2='公共课')
)
insert into Course(CourseCredit2)
select ('专业课') union
select ('公共课') union
select ('专业课') union
select ('公共课') union
select ('专业课') union
select ('公共课') 

select CourseCredit2 from Course
update Course SET CourseCredit='100' where CourseName='公共课'
create table Score
(
ScoreId int primary key identity(1,1),
StuId int foreign key references Student(StuId),
CourseId int foreign key references Course(CourseId),
Score1 decimal(5,2) unique not null
)
select*from  Score
drop table Score
--insert into  Score(CourseId,Score) values('1','100'),('1','100'),('1','100'),('1','100'),('1','100'),('1','100');
insert into Score(Score1)
select ('100')union
select ('90')union
select ('100')union
select ('90')union
select ('100')union
select ('90')union
select ('100')union
select ('90')union
select ('100')union
select ('90')union
select ('100')union
select ('90')union
select ('100')union
select ('90')union
select ('100')union
select ('90')
truncate table Score
delete from  Score WHERE ScoreId=1
DELETE	FROM Score where CourseId=1
alter table Score add constraint fffk  CHECK(Score1>=0 or Score1<=100) 
alter table Course add constraint fqfk default(0)
UPDATE Score SET Score1='10' WHERE CourseId='2'