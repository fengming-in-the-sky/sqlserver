use master
go

create database Students
on
(
	name='Students',
	filename='D:\sql数据库\1\Students.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10Mb
)
log on
(
	name='Students_log',
	filename='D:\sql数据库\1\Students_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10Mb
)
go

use Students
go

create table Class
(
	ClassID int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)

insert into Class (ClassName) values ('一班'),('二班'),('三班'),('四班'),('五班'),('六班'),('七班'),('八班'),('九班'),('十班')
update Class set ClassName='一班多' where ClassID=1
delete from Class where ClassID=10

create table Student
(
	StuID int primary key identity(1,1),
	ClassID int foreign key references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
	StuBirthday date,
	StuPhone nvarchar(11) unique not null,
	StuAddress nvarchar(200),
)
insert into Student (ClassID,StuName,StuSex,Stubirthday,Stuphone) values
('1','我还','男','20000101','12121212121'),
('1','我还1','男','20000101','12121212122'),
('1','我还2','男','20000101','12121212123'),
('1','我还3','男','20000101','12121212125'),
('1','我还4','男','20000101','12121212126'),
('1','我还5','男','20000101','12121212127'),
('1','我还6','男','20000101','12121212128'),
('1','我还7','男','20000101','12121212129'),
('1','我还8','男','20000101','12121212130'),
('1','我还9','男','20000101','12121212131'),
('1','我还0','男','20000101','12121212132'),
('1','我还11','男','20000101','12121212133'),
('1','我还12','男','20000101','12121212134'),
('1','我还13','男','20000101','12121212135'),
('1','我还14','男','20000101','12121212136'),
('1','我还15','男','20000101','12121212137'),
('1','我还16','男','20000101','12121212138'),
('1','我还17','男','20000101','12121212139'),
('1','我还18','男','20000101','12121212140'),
('2','我还19','男','20000101','12121212141')

alter table Student  add CreaDate datetime default(getdate())
update Student set CreaDate='2021/3/10 10:32'
delete from Student where ClassID=1

create table Course
(
	CourseID int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(1<=CourseCredit and CourseCredit<=5),
	CourseType nvarchar(10) check(CourseType='专业课' or CourseType='公共课')
)
insert into Course (CourseName) values
('音乐'),
('思修'),
('美术'),
('体育'),
('数学'),
('英语')
select CourseName from Course
update Course set CourseCredit=4 where CourseName='数学'

create table Score
(
	ScoreID int primary key identity(1,1),
	StuID int foreign key references Student(StuID),
	CourseID int foreign key references Course(CourseID),
	Score decimal(5,2) unique not null 
)
insert into Score (Score) values
('1'),
('52'),
('53'),
('54'),
('55'),
('56'),
('57'),
('58'),
('59'),
('60'),
('70'),
('80'),
('90'),
('10'),
('20'),
('30'),
('40'),
('45'),
('66'),
('77')
update Score set Score=4 where CourseID=3
delete from Score where StuID=1
delete from Score where CourseID=1
alter table Score add constraint UK_Score_Score default(0) for Score,check(1<Score and Score<100)